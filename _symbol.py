import numpy as np
from scipy.spatial.distance import cdist
from stroke import *
from copy import deepcopy
from math import *
import traceback
import matplotlib.pyplot as plt
import sys
from skimage.draw import line_aa
from PIL import Image
import pickle
from itertools import combinations
from sklearn.externals import joblib
import time







class Symbol(object):
	'''An individual symbol in math handwriting (e.g. alphatetic letter, greek letter, math operation etc.)'''

	on_mean,on_std = pickle.load(open('pickled_info/sym_on_stats.pkl','rb'))
	seg_mean,seg_std = pickle.load(open('pickled_info/seg_stats.pkl','rb'))
	rel_mean,rel_std = pickle.load(open('pickled_info/rels_stats.pkl','rb'))
	off_mean,off_std = pickle.load(open('pickled_info/sym_off_stats.pkl','rb'))
	relsPCA = pickle.load(open('pickled_info/rels_hist_pca.pkl','rb'))
	t2k = pickle.load(open('pickled_info/truth_to_key.pkl','rb'))
	k2t = {}
	for k,v in t2k.items():
		k2t[v] = k


	def __init__(self):

		# Whether feature extraction has been done.
		self.didfeats = False
		# Whether the geometry has been simplified.
		self.smoothed = False
		# Top 3 hypothesis
		self.top3 = None
		self.NS = False
		self.name = None
		self.truth = ''
		self.probs = None
		self.normedArea = 1
		self.capital = False

		# Useful Geometry Values
		self.xmax = None
		self.xmin = None
		self.ymax = None
		self.ymin = None
		self.xmed = None
		self.ymed = None

		self.possibleX = False
		self.ttx = False

		# Store Strokes
		self.strokes = []


		self.x = np.array([])
		self.y = np.array([])

		self.stroke_ids = []

		return



	def wipe(self):
		'''Return symbol to initial state'''
		self.Wipe = True
		self.top3 = None
		self.NS = False
		self.name = None
		self.truth = ''
		self.probs = None
		self.fixed = False


		self.xmax = None
		self.xmin = None
		self.ymax = None
		self.ymin = None
		self.xmed = None
		self.ymed = None

		


		self.strokes = []
		self.x = np.array([])
		self.y = np.array([])
		return

	def add_stroke(self, stroke):
		self.strokes.append(stroke)
		self.stroke_ids.append(stroke.id)
		self.setup()



	def setup(self):
		# Call on initialization, computes useful values
		self.x = np.array([])
		self.y = np.array([])
		for stroke in self.strokes:
			stroke.setup()
			self.x = np.append(self.x,stroke.x)
			self.y = np.append(self.y,stroke.y)
		self.xmax = np.max(self.x)
		self.ymax = np.max(self.y)
		self.xmin = np.min(self.x)
		self.ymin = np.min(self.y)
		self.xmed = (self.xmax + self.xmin)/2
		self.ymed = (self.ymax + self.ymin)/2
		self.y75 = (self.ymax + self.ymed)/2
		self.y25 = (self.ymin + self.ymed)/2
		self.x75 = (self.xmax + self.xmed)/2
		self.x25 = (self.xmin + self.xmed)/2
		self.width = self.xmax - self.xmin
		self.height = self.ymax - self.ymin
		self.normal = True
		self.middle = False
		self.ascendant = False
		self.descendant = False

		if self.width == 0:
			self.xmin -= 1e-5
			self.xmax += 1e-5
			self.width = 2e-5

		if self.height == 0:
			self.ymin -= 1e-5
			self.ymax += 1e-5
			self.height = 2e-5
		self.setRefPoint()
		self.area = self.width*self.height

		self.aspectRatio = self.height/self.width


	def fixBadStk(self,md):
		# Combine consecutive strokes if the start of the latter is within md (min distance) of the end of the former.
		if md == 0:
			return []
		r = []
		for i,stk1 in enumerate(self.strokes):
			if len(stk1.x) <= 3:
				for j,stk2 in enumerate(self.strokes):
					if i == j:
						continue
					else:
						d = stk1.closest_distance(stk2)
						if d/md < 0.01:
							r.append(stk1.id)
							break
		ids = []
		subt = 0
		for id_ in r:
			for i,stroke in enumerate(self.strokes):
				if stroke.id == id_:
					ind = i
			del(self.strokes[ind])
			self.setup()
			ids.append(id_)

		return ids


	def draw(self):
		# Create Bitmap
		eqy = 40/(self.height)
		eqx = eqy
		height = 40
		width = int(height/self.aspectRatio)
		if width > 100:
			eqx = 100/self.width
			width = 100
		if width == 0:
			width = 1
			eqx = 1/self.width


		self.normalize(box=[0,width-1,0,height-1],save=False)

		img = np.zeros((height,width),dtype=np.uint8)
		for stroke in self.strokes:
			x = np.copy(stroke.x)
			y = np.copy(stroke.y)
			x = np.round(stroke.x).astype(np.uint8)
			y = np.round(stroke.y).astype(np.uint8)
			for _x,_y,x_,y_ in zip(x,y,x[1:],y[1:]):
				rr, cc, val = line_aa(_x, _y, x_, y_)
				img[cc, rr] = 1


		self.setup()


		return img[::-1]


	def FKIfeatures(self):
		# Offline features for symbol recognition
		img = self.draw()
		img = img.astype(np.float32)
		feats = []
		c3ant = 40 + 1
		c4ant = 0
		for i in range(img.shape[-1]):
			FKI = np.zeros(9)
			column = np.copy(img[:,i]).flatten()
			FKI[0] = (column > 0).sum()
			FKI[1] = np.dot(column,np.linspace(1,40,40))/40
			FKI[2] = np.dot(column,np.linspace(1,40,40)**2)/(40**2)
			nonzero = np.nonzero(column)[0]
			if len(nonzero) == 0:
				FKI[3] = 40
				FKI[4] = 0
			else:
				FKI[3] = nonzero.min()
				FKI[4] = nonzero.max()
			FKI[5] = (FKI[3] - c3ant)/2
			FKI[6] = (FKI[4] - c4ant)/2
			c3ant = FKI[3]
			c4ant = FKI[4]

			b_to_w = np.diff(column)
			FKI[7] = (b_to_w == 1).sum()
			FKI[8] = (column[int(FKI[3]):int(FKI[4])] == 1).sum()
			
			feats.append(FKI)
		feats = np.array(feats)
		self.FKIfeats = feats
		self.FKIfeats = (self.FKIfeats - self.off_mean)/self.off_std


	def setType(self, a):
		self.normal = False
		self.ascendant = False
		self.middle = False
		self.descendant = False

		if a == 'n':
			self.normal = True
		if a == 'a':
			self.ascendant = True
		if a == 'm':
			self.middle = True
		if a == 'd':
			self.descendant = True
		self.setRefPoint()

	def setRefPoint(self):
		# Sets reference point for relation classification
		if self.normal:
			self.refPoint = self.y.mean()
		elif self.ascendant:
			self.refPoint = (self.y.mean() + self.ymin)/2
		elif self.descendant:
			self.refPoint = (self.y.mean() + self.ymax)/2
		elif self.middle:
			self.refPoint = self.ymed



	def getRelsInfo(self,other):

		# Compute features for relation classification (Deprecated).
		self.setRefPoint()
		other.setRefPoint()
		tminx = min(self.xmin,other.xmin)
		tminy = min(self.ymin,other.ymin)
		myinfo = [self.height,self.width,self.x.mean()-tminx,self.y.mean()-tminy]
		otherinfo = [other.height,other.width,other.x.mean()-tminx,other.y.mean()-tminy]
		D = other.refPoint - self.refPoint
		dhc = other.xmed - self.xmed
		dx = other.xmin - self.xmax
		dx1 = other.xmin - self.xmin
		dx2 = other.xmax - self.xmax
		dy = other.ymin - self.ymax
		dy1 = other.ymin - self.ymin
		dy2 = other.ymax - self.ymax
		combined = [D,dhc,dx,dx1,dx2,dy,dy1,dy2]

		return [myinfo,otherinfo,combined]

	def relFeats(self,other, jh=False):
		# Compute features for relation classification.

		self.setRefPoint()
		other.setRefPoint()
		n = 15
		m = 20
		#print(self.xmin,self.xmax)
		#print(other.xmin,other.xmax)


		H = other.height
		D = other.refPoint - self.refPoint
		dhc = other.xmed - self.xmed
		dx = other.xmin - self.xmax
		dx1 = other.xmin - self.xmin
		dx2 = other.xmax - self.xmax
		dy = other.ymin - self.ymax
		dy1 = other.ymin - self.ymin
		dy2 = other.ymax - self.ymax

		geoFeats = [H,D,dhc,dx,dx1,dx2,dy,dy1,dy2]
		geoFeats = np.array(geoFeats)
		F = np.sqrt((self.ymed - other.ymed)**2 + (self.xmed - other.xmed)**2)
		geoFeats /= F

		Ga = np.array([self.x.mean(),self.y.mean()])
		Gb = np.array([other.x.mean(),other.y.mean()])
		G = (Ga + Gb)/2





		other_r = np.sqrt((other.x-G[0])**2 + (other.y-G[1])**2)
		other_theta = np.arctan2(other.y,other.x)

		self_r = np.sqrt((self.x-G[0])**2 + (self.y-G[1])**2)
		self_theta = np.arctan2(self.y,self.x)

		rmax = max(self_r.max(),other_r.max())

		shist,_,_ = np.histogram2d(self_r,self_theta,bins=[n,m],range=[[0,rmax],[-np.pi,np.pi]])
		shist = -shist.flatten()
		ohist,_,_ = np.histogram2d(other_r,other_theta,bins=[n,m],range=[[0,rmax],[-np.pi,np.pi]])
		ohist = ohist.flatten()


		hist = (ohist + shist)
		if not hist.sum() == 0:
			hist = (hist/np.linalg.norm(hist))
		if jh:
			return hist



		hist = hist.reshape(1,-1)
		hist = self.relsPCA.transform(hist).flatten()
		feats = np.append(geoFeats,hist.flatten())
		#feats = (feats - self.rel_mean)/self.rel_std
		return feats



	def newSegmentationFeats(self):
		hist,_,_ = np.histogram2d(self.x,self.y,bins=[10,10],range=[[-1,1],[-1,1]])
		hist = hist.flatten()
		hist = hist/np.linalg.norm(hist)
		hist = (hist - self.seg_mean)/self.seg_std
		return hist



	def segmentationFeats(self):
		# Deprecated
		for stroke in self.strokes:
			stroke.mx = stroke.x.mean()
			stroke.my = stroke.y.mean()



		h = []
		v = []
		d = []
		s = []


		for stk1,stk2 in combinations(self.strokes,2):
			h.append(np.abs(stk1.mx - stk2.mx))
			v.append(np.abs(stk1.my - stk2.my))
			d.append(stk1.closest_distance(stk2))
			s.append(np.abs(stk1.area - stk2.area))

		h = np.array(h).mean()
		v = np.array(v).mean()
		d = np.array(d).mean()
		s = np.array(s).mean()
		feats = np.array([h,v,d,s])
		feats /= (np.sqrt(self.height**2 + self.width**2))
		return feats











	def rotate(self,angle):
		# Rotate all strokes by an angle.
		for stroke in self.strokes:
			stroke.rotate(angle)
		self.setup()
		self.normalize()







	def get_intersect_indicies(self):
		# Determine where strokes intersect each other

		def ccw(A,B,C):
			return (C[1]-A[1]) * (B[0]-A[0]) > (B[1]-A[1]) * (C[0]-A[0])

		def intersect(A,B,C,D):
			return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

		line_segs = []

		int_list = []
		prev = None
		for stroke in self.strokes:
			int_list.append(0)
			for _x,_y,x_,y_ in zip(stroke.x,stroke.y,stroke.x[1:],stroke.y[1:]):
				start = np.array([_x,_y])
				end = np.array([x_,y_])
				intersections = 0
				for seg_start,seg_end in line_segs[:-1]:
					if intersect(start,end,seg_start,seg_end):
						intersections += 1
				int_list.append(intersections)
				line_segs.append((np.array([_x,_y]),np.array([x_,y_])))

		return np.array(int_list)







	def rnnFeatures(self):
		# Online features for rnn classification
		x = np.copy(self.x)
		y = np.copy(self.y)
		dx = np.zeros(len(x))
		dy = np.zeros(len(x))
		ddx = np.zeros(len(x))
		ddy = np.zeros(len(x))
		penup = np.zeros(len(x))
		pos = 0
		for stroke in self.strokes:
			penup[pos+len(stroke.x)-1] = 1
			pos += len(stroke.x)
		tamw = 2
		sigma = 0
		for i in range(1,tamw+1):
			sigma += i*i
		sigma = 2*sigma

		for i in range(len(x)):
			for c in range(1,tamw + 1):
				if i-c <0:
					context_ant_x = x[0]
					context_ant_y = y[0]
				else:
					context_ant_x = x[i-c]
					context_ant_y = y[i-c]

				if i+c >= len(x):
					context_post_x = x[-1]
					context_post_y = y[-1]
				else:
					context_post_x = x[i+c]
					context_post_y = y[i+c]

				dx[i]+=c*(context_post_x-context_ant_x)/sigma
				dy[i]+=c*(context_post_y-context_ant_y)/sigma


		mag_d = np.sqrt(dx**2 + dy**2)
		dx /= mag_d
		dy /= mag_d

		for i in range(len(x)):
			for c in range(1,tamw + 1):
				if i-c <0:
					context_ant_dx = dx[0]
					context_ant_dy = dy[0]
				else:
					context_ant_dx = dx[i-c]
					context_ant_dy = dy[i-c]

				if i+c >= len(x):
					context_post_dx = dx[-1]
					context_post_dy = dy[-1]
				else:
					context_post_dx = dx[i+c]
					context_post_dy = dy[i+c]

				ddx[i]+=c*(context_post_dx-context_ant_dx)/sigma
				ddy[i]+=c*(context_post_dy-context_ant_dy)/sigma


		k = (dx*ddy - ddx*dy)/mag_d*mag_d*mag_d
		int_list = self.get_intersect_indicies()
		self.rnnFeats = np.dstack((x,y,dx,dy,ddx,ddy,k,penup,int_list))[0]
		self.rnnFeats = (self.rnnFeats - self.on_mean)/self.on_std




	def plot(self,scatter=True,sharp=False):
		for stroke in self.strokes:
			plt.plot(stroke.x,stroke.y,color='black')

			if scatter:
				plt.scatter(stroke.x,stroke.y,color='black')



			if sharp:
				for i,xy in stroke.sharpPoints:
					plt.scatter(xy[0],xy[1],color='red')
		plt.xlim((self.xmin-0.05,self.xmax+0.05))
		plt.ylim((self.ymin-0.05,self.ymax+0.05))

		
		return plt


	def closest_distance(self, other):
		XA = np.column_stack((self.x,self.y))
		XB = np.column_stack((other.x,other.y))
		return np.min(cdist(XA,XB))

	def closest_distance_arg(self, other):
		XA = np.column_stack((self.x,self.y))
		XB = np.column_stack((other.x,other.y))
		dist = cdist(XA,XB)
		ind = np.argmin(dist)
		return (np.unravel_index(ind,dist.shape),np.min(dist))


	def add_stroke_coord(self, seq):
		x,y = seq
		self.x = np.append(self.x,x)
		self.y = np.append(self.y,y)
		self.setup()
		s = Stroke([x,y])
		self.strokes.append(s)


	def add_strokes(self, XY):
		X,Y = XY
		for x,y in zip(X,Y):
			self.add_stroke([x,y])


	def get_dist(self, stroke):
		XA = np.column_stack((self.x,self.y))
		XB = np.column_stack((stroke.x,stroke.y))
		return np.min(cdist(XA,XB))

	def get_angle_dist(self, stroke):
		dx = np.add(stroke.x,-self.xmed)
		dy = np.add(stroke.y,-self.ymed)
		r = np.sqrt(dx**2 + dy**2)
		r0 = np.where(r==0)
		r = np.delete(r,r0)
		theta = np.arctan2(dy,dx)
		theta = np.delete(theta,r0)
		np.putmask(theta,theta<0,2*np.pi+theta)
		return [r,theta]


	def checkInBox(self, box):
		xmin,xmax,ymin,ymax = box
		xmed = (xmax + xmin)/2
		ymed = (ymax + ymin)/2
		width = xmax-xmin
		height = ymax-ymin
		return (abs(self.xmed - xmed) < (width + self.width)) and (abs(self.ymed - ymed) < (height + self.height))



	def getMinDist(self, other):
		d = []
		for stk1 in self.strokes:
			for stk2 in other.strokes:
				d.append(self.strokeMinD[stk1.id][stk2.id])

		return min(d)


	def normalize(self, box=[-1,1,-1,1],save=True):
		if save:
			for stroke in self.strokes:
				stroke.orig_x = np.copy(stroke.x)
				stroke.orig_y = np.copy(stroke.y)

		in_width = self.width
		in_height = self.height
			
		if in_width > in_height:
			self.ymin = ((self.ymax + self.ymin)/2.) - in_width/2.
			self.ymax = self.ymin + in_width
		else:
			self.xmin = ((self.xmax + self.xmin)/2.) - in_height/2.
			self.xmax = self.xmin + in_height
		in_width = self.xmax - self.xmin
		in_height = self.ymax - self.ymin

		out_width = box[1] - box[0]
		out_height = box[3] - box[2]

		if in_width == 0:
			self.xmin -= 0.01
			self.xmax += 0.01
			in_width = 0.02

		if in_height == 0:
			self.ymin -= 0.01
			self.ymax += 0.01
			in_height = 0.02
		for stroke in self.strokes:
			stroke.x = ((stroke.x - self.xmin) / in_width) * out_width + box[0]
			stroke.y = ((stroke.y - self.ymin) / in_height) * out_height + box[2]
			stroke.setup()
		self.setup()
		return


	def remove_hooks(self):
		for stk in self.strokes:
			for i in range(5):
				stk.remove_hooks()
		self.setup()


	def getOriginalPoints(self):


		for stroke in self.strokes:
			stroke.x = stroke.ox[:]
			stroke.y = stroke.oy[:]
			stroke.setup()

		self.setup()

		return




	def resetCoords(self):
		for stroke in self.strokes:
			stroke.x = np.copy(stroke.orig_x)
			stroke.y = np.copy(stroke.orig_y)
			del stroke.orig_x,stroke.orig_y
			stroke.setup()
		self.setup()





	def getAspectRatio(self):

		h = self.y.max() - self.y.min()
		w = self.x.max() - self.x.min()

		h = max(h,0.01)
		w = max(w,0.01)

		if w > h:
			return w/h - 1
		else:
			return 1 - h/w






	def best_smooth(self):
		xmin,xmax,ymin,ymax = self.xmin,self.xmax,self.ymin,self.ymax
		self.normalize()
		if self.smoothed:
			return

		else:
			self.smoothed = True
			for stroke in self.strokes:
				for i in range(5):
					stroke.remove_hooks()
				stroke.applySmoothing()

		self.normalize(box=[xmin,xmax,ymin,ymax])
		self.setup()



	def svmFeatures(self):
		self.t = 0
		self.setup()
		if self.didfeats:
			return

		else:
			self.didfeats = True
			for stroke in self.strokes:
				stroke.oldx = stroke.x[:]
				stroke.oldy = stroke.y[:]


			features = []

			self.t = time.time()
			features += self.newGetCrossings()
			features += [len(self.strokes)]
			features += self.lineFeatures()
			features += self.hist2d()
			features += self.gabor()
			features += [self.getAspectRatio()]
			features += self.eigenFeatures()
			features.append(len(self.strokes))
			features.append(self.width)
			features.append(self.height)
			self.Feats = features
			self.Feats = np.array(self.Feats)
			for stroke in self.strokes:
				stroke.x = stroke.oldx[:]
				stroke.y = stroke.oldy[:]
				stroke.setup()
			self.setup()
			return




	def eigenFeatures(self):

		var_x = ((self.x - self.x.mean())**2).mean()
		var_y = ((self.y - self.y.mean())**2).mean()
		cov = ((self.y - self.y.mean())*((self.x - self.x.mean()))).mean()


		return [var_x, var_y, cov]



	def hist2d(self):
		rows = 5
		cols = 5

		hist = np.zeros(25)
		for stroke in self.strokes:
			hist += stroke.histogram_2d(rows,cols)
		return (hist/len(self.x)).tolist()



	def hist2dfuzzy(self):

		xc = np.linspace(-1,1,5)
		yc = np.linspace(-1,1,5)
		h = []
		for x in xc:
			for y in yc:
				m = ((self.width - np.abs(self.x-x))/self.width)*((self.height - np.abs(self.y-y))/self.height).sum()
				h.append(m)
		h = np.array(h)
		return h/np.linalg.norm(h)


	def gabor(self):
		rows = 3
		cols = 3

		lens = []
		gabors = []
		sym_gabor = np.zeros(4*rows*cols)

		totlen = 0.

		for stroke in self.strokes:
			tgab,tlen = stroke.getGabor(rows,cols)
			gabors.append(tgab)
			lens.append(tlen)
			totlen += tlen

		if tlen > 0:
			for t in range(len(self.strokes)):
				w = lens[t]/totlen

				sym_gabor += w*gabors[t]

		return sym_gabor.tolist()




		return gabor


	def lineFeatures(self):
		features = []
		linefeatures = np.array([0.,0.,0.])
		for stroke in self.strokes:
			linefeatures += stroke.lineCumulativeFeatures()
		currentLineFeatures = linefeatures
		cumulative = linefeatures/len(self.strokes)
		return linefeatures.tolist() + (linefeatures/len(self.strokes)).tolist()

	def getCrossings(self):
		features = []
		numCrossings = 5
		numSubCrossings = 9
		x,y = (np.copy(self.x),np.copy(self.y))
		horizontal_count_crossings = []
		horizontal_min_crossings = []
		horizontal_max_crossings = []
		vertical_count_crossings = []
		vertical_min_crossings = []
		vertical_max_crossings = []
		xb = x[1:]
		yb = y[1:]
		xa = x[:-1]
		ya = y[:-1]

		step = 2/(numCrossings + 1)
		subStep = step/(numSubCrossings + 1)
		init = np.linspace(-1,1,)
		for i in range(1,numCrossings+1):
			h_crossings = [0,0,1.1,-1.1]
			v_crossings = [0,0,1.1,-1.1]

			init = -1 + i*step

			total_h_crossings = 0
			avg_x = 0
			avg_x_min = 0
			avg_x_max = 0
			total_v_crossings = 0
			avg_y = 0
			avg_y_min = 0
			avg_y_max = 0
			for k in range(1, numSubCrossings + 1):

				init = -1 + (i-0.5)*step + k*subStep
				#for horizontals

				x_index = np.where(np.logical_or(np.logical_and(yb >= init,ya <= init),np.logical_and(ya >= init,yb <= init)))


				Xa = xa[x_index]
				Xb = xb[x_index]
				Ya = ya[x_index]
				Yb = yb[x_index]

				m = (Yb - Ya)/(Xb - Xa)
				m = np.nan_to_num(m)
				m[m==0] = 1e-10

				b = Yb - m*Xb

				x_point = (init - b)/m


				y_index = np.where(np.logical_or(np.logical_and(xb >= init,xa <= init),np.logical_and(xa >= init,xb <= init)))

				Xa = xa[y_index]
				Xb = xb[y_index]
				Ya = ya[y_index]
				Yb = yb[y_index]

				m = (Yb - Ya)/(Xb - Xa)
				m = np.nan_to_num(m)


				b = Yb - m*Xb

				y_point = m*init + b

				total_h_crossings += len(x_point)
				total_v_crossings += len(y_point)

				if len(x_point) > 0:
					avg_x_min += x_point.min()
					avg_x_max += x_point.max()
					avg_x += x_point.sum()
				else:
					avg_x_min += 1.1
					avg_x_max += -1.1

				if len(y_point) > 0:
					avg_y_min += y_point.min()
					avg_y_max += y_point.max()
					avg_y += y_point.sum()
				else:
					avg_y_min += 1.1
					avg_y_max += -1.1

			h_crossings[0] = round((total_h_crossings*2)/numSubCrossings)/2
			if total_h_crossings > 0:
				h_crossings[1] = avg_x / total_h_crossings
				h_crossings[2] = avg_x_min / numSubCrossings
				#print(avg_x_min)
				h_crossings[3] = avg_x_max / numSubCrossings

			v_crossings[0] = round((total_v_crossings * 2)/numSubCrossings)/2
			if total_v_crossings > 0:
				v_crossings[1] = avg_y / total_v_crossings
				v_crossings[2] = avg_y_min / numSubCrossings
				v_crossings[3] = avg_y_max / numSubCrossings


			horizontal_count_crossings.append(h_crossings[0])
			horizontal_min_crossings.append(h_crossings[2])
			horizontal_max_crossings.append(h_crossings[3])

			vertical_count_crossings.append(v_crossings[0])
			vertical_min_crossings.append(v_crossings[2])
			vertical_max_crossings.append(v_crossings[3])




		features += horizontal_count_crossings
		features += horizontal_min_crossings
		features += horizontal_max_crossings

		features += vertical_count_crossings
		features += vertical_min_crossings
		features += vertical_max_crossings
		return features





	def newGetCrossings(self):
		features = []
		numCrossings = 5
		numSubCrossings = 9
		X = []
		Y = []
		for stroke in self.strokes:
			X.append(stroke.x)
			Y.append(stroke.y)
		horizontal_count_crossings = []
		horizontal_min_crossings = []
		horizontal_max_crossings = []
		vertical_count_crossings = []
		vertical_min_crossings = []
		vertical_max_crossings = []
		XB = []
		YB = []
		XA = []
		YA = []
		for _x_,_y_ in zip(X,Y):
			XB.append(np.copy(_x_[1:]))
			XA.append(np.copy(_x_[:-1]))
			YB.append(np.copy(_y_[1:]))
			YA.append(np.copy(_y_[:-1]))

		step = 2/(numCrossings + 1)
		subStep = step/(numSubCrossings + 1)
		for i in range(1,numCrossings+1):
			h_crossings = [0,0,1.1,-1.1]
			v_crossings = [0,0,1.1,-1.1]

			init = -1 + i*step

			total_h_crossings = 0
			avg_x = 0
			avg_x_min = 0
			avg_x_max = 0
			total_v_crossings = 0
			avg_y = 0
			avg_y_min = 0
			avg_y_max = 0
			for k in range(1, numSubCrossings + 1):

				init = -1 + (i-0.5)*step + k*subStep
				#for horizontals
				Xa = np.array([])
				Xb = np.array([])
				Ya = np.array([])
				Yb = np.array([])
				for xb,xa,yb,ya in zip(XB,XA,YB,YA):
					x_index = np.where(np.logical_or(np.logical_and(yb >= init,ya <= init),np.logical_and(ya >= init,yb <= init)))
					Xa = np.append(Xa,xa[x_index])
					Xb = np.append(Xb,xb[x_index])
					Ya = np.append(Ya,ya[x_index])
					Yb = np.append(Yb,yb[x_index])

				m = (Yb - Ya)/(Xb - Xa + 1e-10)
				m = np.nan_to_num(m)
				np.clip(m, -1e10, 1e10)
				m[m==0] = 1e-10

				b = Yb - m*Xb

				x_point = (init - b)/m

				Xa = np.array([])
				Xb = np.array([])
				Ya = np.array([])
				Yb = np.array([])
				for xb,xa,yb,ya in zip(XB,XA,YB,YA):
					y_index = np.where(np.logical_or(np.logical_and(xb >= init,xa <= init),np.logical_and(xa >= init,xb <= init)))
					Xa = np.append(Xa,xa[y_index])
					Xb = np.append(Xb,xb[y_index])
					Ya = np.append(Ya,ya[y_index])
					Yb = np.append(Yb,yb[y_index])

				m = (Yb - Ya)/(Xb - Xa + 1e-10)
				m = np.nan_to_num(m)
				np.clip(m, -1e10, 1e10)
				b = Yb - m*Xb

				y_point = m*init + b

				total_h_crossings += len(x_point)
				total_v_crossings += len(y_point)

				if len(x_point) > 0:
					avg_x_min += x_point.min()
					avg_x_max += x_point.max()
					avg_x += x_point.sum()
				else:
					avg_x_min += 1.1
					avg_x_max += -1.1

				if len(y_point) > 0:
					avg_y_min += y_point.min()
					avg_y_max += y_point.max()
					avg_y += y_point.sum()
				else:
					avg_y_min += 1.1
					avg_y_max += -1.1

			h_crossings[0] = round((total_h_crossings*2)/numSubCrossings)/2
			if total_h_crossings > 0:
				h_crossings[1] = avg_x / total_h_crossings
				h_crossings[2] = avg_x_min / numSubCrossings
				#print(avg_x_min)
				h_crossings[3] = avg_x_max / numSubCrossings

			v_crossings[0] = round((total_v_crossings * 2)/numSubCrossings)/2
			if total_v_crossings > 0:
				v_crossings[1] = avg_y / total_v_crossings
				v_crossings[2] = avg_y_min / numSubCrossings
				v_crossings[3] = avg_y_max / numSubCrossings


			horizontal_count_crossings.append(h_crossings[0])
			horizontal_min_crossings.append(h_crossings[2])
			horizontal_max_crossings.append(h_crossings[3])

			vertical_count_crossings.append(v_crossings[0])
			vertical_min_crossings.append(v_crossings[2])
			vertical_max_crossings.append(v_crossings[3])




		features += horizontal_count_crossings
		features += horizontal_min_crossings
		features += horizontal_max_crossings

		features += vertical_count_crossings
		features += vertical_min_crossings
		features += vertical_max_crossings
		return features











class C(Symbol):
	def __init__(self):
		Symbol.__init__(self)
		self.type = 'C'
	def setup(self):
		Symbol.setup(self)



class G(Symbol):
	def __init__(self):
		Symbol.__init__(self)
		self.type = 'G'
	def setup(self):
		Symbol.setup(self)

class B(Symbol):
	def __init__(self):
		Symbol.__init__(self)
		self.type = 'B'
	def setup(self):
		Symbol.setup(self)


class M(Symbol):
	def __init__(self):
		Symbol.__init__(self)
		self.type = 'M'

	def setup(self):
		Symbol.setup(self)



class S(Symbol):
	def __init__(self):
		Symbol.__init__(self)
		self.type = 'S'

	def setup(self):
		Symbol.setup(self)


class F(Symbol):
	def __init__(self):
		Symbol.__init__(self)
		self.type = 'F'
	def setup(self):
		Symbol.setup(self)
		if self.truth in ['\\sin','\\cos','\\tan']:
			self.subtype = 'trig'
		else:
			self.subtype = 'log'

		return


class L(Symbol):
	def __init__(self):
		Symbol.__init__(self)
		self.type = 'L'
	def setup(self):
		Symbol.setup(self)


class R(Symbol):
	def __init__(self):
		Symbol.__init__(self)
		self.type = 'R'
	def setup(self):
		Symbol.setup(self)


class P(Symbol):
	#[')', ']', '|', '(', '\\{', '[', '\\}']



	def __init__(self):
		Symbol.__init__(self)
		self.type = 'P'

	def setup(self):
		Symbol.setup(self)
		if self.truth in ['(',')']:
			self.subtype = 'R'
		elif self.truth in ['[',']']:
			self.subtype = 'B'
		elif self.truth in ['\\{','\\}']:
			self.subtype = 'C'
		elif self.truth in ['|']:
			self.subtype = 'A'
			self.side = 'B'
		if self.truth in ['(','[','\\{']:
			self.side = 'L'
		elif self.truth != '|':
			self.side = 'R'
		return

class N(Symbol):
	def __init__(self):
		Symbol.__init__(self)
		self.type = 'N'

	def setup(self):
		Symbol.setup(self)


class O(Symbol):
	def __init__(self):
		Symbol.__init__(self)
		self.type = 'O'

	def setup(self):
		self.ChangedLength = False
		Symbol.setup(self)


class X(Symbol):
	def __init__(self):
		Symbol.__init__(self)
		self.type = 'X'
	def setup(self):
		Symbol.setup(self)
