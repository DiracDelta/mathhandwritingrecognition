import sys
sys.path.append('..')
from stroke import Stroke
from _symbol import Symbol
from .classifiers import SymCLF as symclf
from .classifiers import RelCLF as relclf
import numpy as np
import pickle
from .tablecyk import *
from skimage.draw import line_aa
import matplotlib.pyplot as plt
from .meparser import *
import time
class Sample(object):


	SymCLF = symclf()
	RelCLF = relclf()


	def __init__(self,strokes,strokeOrder, meParse):
		
		self.meParse = meParse
		self.strokes = {}
		self.strokeOrder = strokeOrder
		self.symbols = {}
		self.symbols1 = {}
		self.symbolOrder = []
		self.subExpressions = {}
		self.Fractions = {}
		self.Summations = {}
		self.Integrals = {}
		self.Products = {}
		self.SquareRoots = {}
		self.strokeMinDist = {}
		self.strokeMinDistOrder = {}
		self.strokeMedDist = {}
		self.strokeMedDistOrder = {}
		self.strokeLeftOf = {}
		self.strokeRightOf = {}
		self.strokeHorizontalInBetween = {}
		self.strokeVerticalInBetween = {}
		self.strokeAbove = {}
		self.strokeMinDistInd = {}
		self.strokeBelow = {}
		self.possibleFracs = []
		self.sums = []
		self.curlyX = []
		self.ints = []
		self.strokeBoundingBoxIntersecting = {}
		self.strokeInBetweenOrder = {}
		self.symMinDist = {}
		self.strokeToSymGroup = {}
		self.strokeToSymProbs = {}
		self.rels = {}
		self.checked = []
		self.strokeToSymID = {}
		self.possibleSyms = {}
		self.fixedSyms = {}
		self.symProbs = {}
		self.possibleInts = []
		self.j = 0
		self.symProbs = {}
		self.numSyms = {}
		self.torder = []
		self.didPen = set()
		self.penVals = {}
		for s_id in self.strokeOrder:
			self.torder.append(s_id)
			self.add(s_id,strokes)
			self.updateSpatialDictionaries(s_id)

		self.render()
		self.visibility()


	def add(self, s_id, strokes):
		self.strokes[s_id] = strokes[s_id]
		Sym = Symbol()
		Sym.name = '(%s)' % s_id
		Sym.add_stroke(self.strokes[s_id])
		strokes[s_id].sym1 = Sym
		self.meParse.SymCLF.getSymProbs(Sym)
		self.symbols1[s_id] = self.strokes[s_id].sym1
		# if len(self.torder) == 1 or self.strokes[self.torder[-2]].skipCheck:
		# 	self.meParse.SymCLF.getSymProbs(Sym)
		# else:
		# 	lsym = self.strokes[self.torder[-2]].sym1	
		# 	self.meParse.SymCLF.getSymProbs(Sym, lsym)
		# self.symbols1[s_id] = self.strokes[s_id].sym1
		# if self.strokes[s_id].sym1.possibleX:
		# 	self.symbols1[self.torder[-2]] = self.strokes[s_id].sym1
		





	def updateSpatialDictionaries(self, s_id):

		self.strokeMinDist[s_id] = {}
		self.strokeMinDistInd[s_id] = {}
		self.strokeMinDistOrder[s_id] = []
		#self.strokeMedDist[s_id] = {}
		#self.strokeMedDistOrder[s_id] = []
		#self.strokeInBetweenOrder[s_id] = []
		#self.strokeBoundingBoxIntersecting[s_id] = []
		#self.strokeLeftOf[s_id] = []
		#self.strokeRightOf[s_id] = []
		#self.strokeHorizontalInBetween[s_id] = []
		#self.strokeVerticalInBetween[s_id] = []
		#self.strokeAbove[s_id] = []
		#self.strokeBelow[s_id] = []

		if len(self.torder) == 1:
			return
		stroke1 = self.strokes[s_id]


		for s_id2 in self.torder[:-1]:
			#MinDist
			stroke2 = self.strokes[s_id2]
			ind, d = stroke1.closest_distance_arg(stroke2)
			ind1, ind2 = ind
			self.strokeMinDistInd[s_id][s_id2] = ind1
			self.strokeMinDistInd[s_id2][s_id] = ind2
			self.strokeMinDist[s_id][s_id2] = d
			self.strokeMinDist[s_id2][s_id] = d
			self.strokeMinDistOrder[s_id].append(s_id2)
			self.strokeMinDistOrder[s_id].sort(key=lambda x: self.strokeMinDist[s_id][x])
			self.strokeMinDistOrder[s_id2].append(s_id)
			self.strokeMinDistOrder[s_id2].sort(key=lambda x: self.strokeMinDist[s_id2][x])


			#MedDist
			# d = stroke1.center_distance(stroke2)
			# self.strokeMedDist[s_id][s_id2] = d
			# self.strokeMedDist[s_id2][s_id] = d
			# self.strokeMedDistOrder[s_id].append(s_id2)
			# self.strokeMedDistOrder[s_id].sort(key=lambda x: self.strokeMedDist[s_id][x])
			# self.strokeMedDistOrder[s_id2].append(s_id)
			# self.strokeMedDistOrder[s_id2].sort(key=lambda x: self.strokeMedDist[s_id2][x])


			# #BoundingBox
			# if stroke1.bb_intersects(stroke2):
			# 	self.strokeBoundingBoxIntersecting[s_id].append(s_id2)
			# 	self.strokeBoundingBoxIntersecting[s_id].sort(key=lambda x: self.strokeMinDist[s_id][x])
			# 	self.strokeBoundingBoxIntersecting[s_id2].append(s_id)
			# 	self.strokeBoundingBoxIntersecting[s_id2].sort(key=lambda x: self.strokeMinDist[s_id2][x])




			# if stroke1.rightOf(stroke2):
			# 	self.strokeLeftOf[s_id].append(s_id2)
			# 	self.strokeRightOf[s_id2].append(s_id)
			# 	self.strokeLeftOf[s_id].sort(key=lambda x: self.strokeMinDist[s_id][x])
			# 	self.strokeRightOf[s_id2].sort(key=lambda x: self.strokeMinDist[s_id2][x])
			# elif stroke1.leftOf(stroke2):
			# 	self.strokeRightOf[s_id].append(s_id2)
			# 	self.strokeLeftOf[s_id2].append(s_id)
			# 	self.strokeRightOf[s_id].sort(key=lambda x: self.strokeMinDist[s_id][x])
			# 	self.strokeLeftOf[s_id2].sort(key=lambda x: self.strokeMinDist[s_id2][x])
			# else:
			# 	self.strokeHorizontalInBetween[s_id].append(s_id2)
			# 	self.strokeHorizontalInBetween[s_id2].append(s_id)
			# 	self.strokeHorizontalInBetween[s_id].sort(key=lambda x: self.strokeMinDist[s_id][x])
			# 	self.strokeHorizontalInBetween[s_id2].sort(key=lambda x: self.strokeMinDist[s_id2][x])
			# if stroke1.above(stroke2):
			# 	self.strokeAbove[s_id].append(s_id2)
			# 	self.strokeBelow[s_id2].append(s_id)
			# 	self.strokeAbove[s_id].sort(key=lambda x: self.strokeMinDist[s_id][x])
			# 	self.strokeBelow[s_id2].sort(key=lambda x: self.strokeMinDist[s_id2][x])
			# elif stroke1.below(stroke2):
			# 	self.strokeBelow[s_id].append(s_id2)
			# 	self.strokeAbove[s_id2].append(s_id)
			# 	self.strokeBelow[s_id].sort(key=lambda x: self.strokeMinDist[s_id][x])
			# 	self.strokeAbove[s_id2].sort(key=lambda x: self.strokeMinDist[s_id2][x])
			# else:
			# 	self.strokeVerticalInBetween[s_id].append(s_id2)
			# 	self.strokeVerticalInBetween[s_id2].append(s_id)
			# 	self.strokeVerticalInBetween[s_id].sort(key=lambda x: self.strokeMinDist[s_id][x])
			# 	self.strokeVerticalInBetween[s_id2].sort(key=lambda x: self.strokeMinDist[s_id2][x])





	def render(self):
		xmin = 1e20
		xmax = -1e20
		ymin = 1e20
		ymax = -1e20
		for s_id in self.strokeOrder:
			stroke = self.strokes[str(s_id)]
			if stroke.xmin < xmin:
				xmin = stroke.xmin
			if stroke.ymin < ymin:
				ymin = stroke.ymin
			if stroke.xmax > xmax:
				xmax = stroke.xmax
			if stroke.ymax > ymax:
				ymax = stroke.ymax



		W = xmax - xmin
		H = ymax - ymin

		R = W/H
		eqy = 256/(H)
		eqx = eqy
		H = 256
		W = int(H*R)
		if W == 0:
			W = 1

		img = np.zeros((H+10,W+10),dtype=np.uint8)
		strokePixel = img[:] + -1
		for stroke in self.strokes.values():
			x = stroke.x[:] - xmin
			y = stroke.y[:] - ymin
			x *= eqx
			y *= eqy
			x = x.astype(np.int32) + 5
			y = y.astype(np.int32) + 5
			for _x,_y,x_,y_ in zip(x,y,x[1:],y[1:]):
				rr, cc, val = line_aa(_x, _y, x_, y_)
				img[cc, rr] = 255
				strokePixel[cc, rr] = int(stroke.id)

		self.xmin = xmin
		self.ymin = ymin
		self.xmax = xmax
		self.ymax = ymax
		self.img = img
		self.eqx = eqx
		self.eqy = eqy
		self.strokePixel = strokePixel











	def setRegion(self, c, nstks):
		c.ccc[nstks] = True
		xmin = 1e10
		xmax = -1e10
		ymin = 1e10
		ymax = -1e10
		for s_id in nstks:
			stroke = self.strokes[str(s_id)]
			if stroke.xmin < xmin:
				xmin = stroke.xmin
			if stroke.ymin < ymin:
				ymin = stroke.ymin
			if stroke.xmax > xmax:
				xmax = stroke.xmax
			if stroke.ymax > ymax:
				ymax = stroke.ymax


		c.xmin = xmin
		c.ymin = ymin
		c.xmax = xmax
		c.ymax = ymax
		c.coo = coo(c.xmin,c.ymin,c.xmax,c.ymax)
		c.coo.cell = c



	def getAvgSize(self):
		self.avgH = sum(map(lambda x: self.strokes[self.strokeOrder[x]].height, self.strokeOrder))/len(self.strokeOrder)
		self.avgW = sum(map(lambda x: self.strokes[self.strokeOrder[x]].width, self.strokeOrder))/len(self.strokeOrder)


	def getDist(self, i, j):
		return self.strokeMinDist[self.strokeOrder[i]][self.strokeOrder[j]]



	def visibility(self):

		self.strokeVisibilityBool = {}
		for i in range(len(self.strokeOrder)):
			sid_1 = self.strokeOrder[i]
			self.strokeVisibilityBool[sid_1] = {}

		# for i in range(len(self.strokeOrder)):
		# 	sid_1 = self.strokeOrder[i]
		# 	for j in range(i+1,len(self.strokeOrder)):
		# 		sid_2 = self.strokeOrder[j]

		# 		if sid_2 in self.strokeMinDistOrder[sid_1][:4] and sid_1 in self.strokeMinDistOrder[sid_2][:4]:
		# 			self.strokeVisibilityBool[sid_1][sid_2] = True
		# 			self.strokeVisibilityBool[sid_2][sid_1] = True
		# 		else:
		# 			self.strokeVisibilityBool[sid_1][sid_2] = False
		# 			self.strokeVisibilityBool[sid_2][sid_1] = False

		# return










		H,W = self.img.shape
		dl = 3.125e-4
		for i in range(len(self.strokeOrder)):
			sid_1 = self.strokeOrder[i]
			for j in range(i+1, len(self.strokeOrder)):
				sid_2 = self.strokeOrder[j]
				p1x,p1y = (self.strokes[sid_1].x[[self.strokeMinDistInd[sid_1][sid_2]]],self.strokes[sid_1].y[[self.strokeMinDistInd[sid_1][sid_2]]])
				p2x,p2y = (self.strokes[sid_2].x[[self.strokeMinDistInd[sid_2][sid_1]]],self.strokes[sid_2].y[[self.strokeMinDistInd[sid_2][sid_1]]])

				p1x = int((p1x - self.xmin)*self.eqx) + 5
				p1y = int((p1y - self.ymin)*self.eqy) + 5

				p2x = int((p2x - self.xmin)*self.eqx) + 5
				p2y = int((p2y - self.ymin)*self.eqy) + 5

				dx = p2x - p1x
				dy = p2y - p1y
				l = 0
				V = True
				while l < 1:
					x = p1x + int(dx*l + 0.5)
					y = p1y + int(dy*l + 0.5)
					if self.img[y][x] == 255 and not self.strokePixel[y][x] in (i,j):
						V = False
						break
					l += dl

				self.strokeVisibilityBool[sid_1][sid_2] = V
				self.strokeVisibilityBool[sid_2][sid_1] = V



	def visible(self, i , j):
		return self.strokeVisibilityBool[self.strokeOrder[j]][self.strokeOrder[i]]

	def groupPenalty(self, A, B):
		t = str(A) + str(B)

		if t in self.didPen:
			return self.penVals[t]
		t2 = str(B) + str(A)


		dmin = 1e50

		a = np.argwhere(A.ccc).flatten()
		b = np.argwhere(B.ccc).flatten()
		for i in a:
			for j in b:
				dmin = min(dmin, self.getDist(i,j))


		self.penVals[t] = dmin
		self.penVals[t2] = dmin
		self.didPen.add(t)
		self.didPen.add(t2)

		return dmin








	def searchBox(self, box, check):


		ymax,ymin,xmax,xmin = box

		for s_id in check:
			stroke = self.strokes[s_id]
			if (xmin <= stroke.xmed <= xmax) and (ymin <= stroke.ymed <= ymax):
				return True

		return False






	def isLegal(self, s_ids):




		if len(s_ids) == 2:
			if not self.getDist(int(s_ids[0]),int(s_ids[1])) <= self.distance_th:
				return False


		check = self.strokeOrder[:]
		for s_id in s_ids:
			check.remove(s_id)



		s_ids.sort(key = lambda x: self.strokes[x].xmin)

		for i in range(len(s_ids)):
			stroke1 = self.strokes[s_ids[i]]
			for j in range(i+1,len(s_ids)):
				stroke2 = self.strokes[s_ids[j]]
				if self.getDist(int(s_ids[i]),int(s_ids[j])) <= self.distance_th:
					ymax = min(stroke1.ymax,stroke2.ymax)
					ymin = max(stroke1.ymin,stroke2.ymin)
					xmax = min(stroke1.xmax,stroke2.xmax)
					xmin = max(stroke1.xmin,stroke2.xmin)
				else:
					ymax = max(stroke1.ymax,stroke2.ymax)
					ymin = min(stroke1.ymin,stroke2.ymin)
					xmax = max(stroke1.xmax,stroke2.xmax)
					xmin = min(stroke1.xmin,stroke2.xmin)

				if self.searchBox((ymax,ymin,xmax,xmin),check):
					return False
		return True








	def getCloseStrokes(self, id_, dist_th):
		added = set()
		close = []

		updated = False

		for i in range(int(id_)):

			if self.getDist(int(id_),int(i)) < dist_th:
				close.append(i)
				added.add(i)


		updated = True
		auxlist = []

		for i in close:
			for j in range(i):
				if (not j in added) and self.getDist(i,j) < dist_th:
					auxlist.append(j)
					added.add(j)


		return close + auxlist






	def getRefSymbol(self):


		area = 0
		RX = 0
		RY = 0
		vmedx = []
		vmedy = []
		nreg = 0
		for stroke in self.strokes.values():

			vmedx.append(stroke.width)
			vmedy.append(stroke.height)

			area += stroke.area

			if stroke.aspect >= 0.25 and stroke.aspect <= 4:
				RX += stroke.width
				RY += stroke.height
				nreg += 1



		area /= len(vmedx)
		area = np.sqrt(area) + 0.5
		area *= 0.9
		if nreg == 0:
			nreg = 1
		RX /= nreg
		RY /= nreg







		vmedx = np.array(vmedx)
		vmedy = np.array(vmedy)


		vmedy.sort()
		vmedx.sort()



		self.RX = (RX + vmedx[int(len(vmedx)/2)] + area)/3
		self.RY = (RY + vmedy[int(len(vmedy)/2)] + area)/3


		norm = np.sqrt(self.RY**2 + self.RX**2)
		for i in range(len(self.strokeOrder)):
			k = self.strokeOrder[i]
			for j in range(i+1, len(self.strokeOrder)):
					k2 = self.strokeOrder[j]
					
					if not self.strokeVisibilityBool[k][k2]:
						self.strokeMinDist[k][k2] = np.inf
						self.strokeMinDist[k2][k] = self.strokeMinDist[k][k2]

					else:

						self.strokeMinDist[k][k2] = self.strokeMinDist[k][k2]/norm
						self.strokeMinDist[k2][k] = self.strokeMinDist[k][k2]



















