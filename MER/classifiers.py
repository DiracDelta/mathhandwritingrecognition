import sys
sys.path.append('../')
from stroke import *
from _symbol import *
import numpy as np
import pickle
import string
import random
import numpy as np
import time
from sklearn.externals import joblib
from keras.models import model_from_json
import os

class SegCLF(object):

	SegCLF = joblib.load('pickled_info/seg_clf.pkl')
	# json_file = open('pickled_info/sym_seg/model.json', 'r')
	# loaded_model_json = json_file.read()
	# json_file.close()
	# SegCLF = model_from_json(loaded_model_json)
	# SegCLF.load_weights("pickled_info/sym_seg/weights.h5")

	def __init__(self):
		return

	def getProb(self, sym):

		if len(sym.strokes) == 1:
			return 1

		feats = sym.segmentationFeats()
		return self.SegCLF.predict_proba(feats.reshape(1,-1))[0][0]
		








class SymCLF(object):
	types = pickle.load(open('pickled_info/sym_types.pkl','rb'))
	json_file = open('pickled_info/sym_clf/model.json', 'r')
	loaded_model_json = json_file.read()
	json_file.close()
	SymCLF = model_from_json(loaded_model_json)
	SymCLF.load_weights("pickled_info/sym_clf/weights.h5")
	symT2k = pickle.load(open('pickled_info/truth_to_key.pkl','rb'))
	symK2t = {}
	for k,v in symT2k.items():
		symK2t[v] = k
	segClf = SegCLF()
	numStroke = pickle.load(open('pickled_info/num_strokes.pkl','rb'))
	on_len = 200
	off_len = 100

	for k,v in numStroke.items():
		t = v['total']
		for k2 in v.keys():
			if k2 == 'total':
				continue
			else:
				if v[k2] == 0:
					v[k2] = 1
				v[k2] /= t


	def __init__(self):
		self.N = 5
		self.time = 0
		self.ids = set()
		return

	def id_generator(self,size=5, chars=string.ascii_lowercase):
		t_id = ''.join(random.choice(chars) for _ in range(size))
		if t_id in self.ids:
			return self.id_generator()
		else:
			self.ids.add(t_id)
			return t_id

	def getSymProbs(self,sym, last=None):
		ti = time.time()

		sym.normalize()
		sym.rnnFeatures()
		sym.FKIfeatures()

		diff = self.on_len - len(sym.rnnFeats)
		if diff > 0:
			addit = np.zeros(9*diff).reshape((diff,9))
			sym.rnnFeats = np.vstack((sym.rnnFeats, addit))
		else:
			sym.rnnFeats = sym.rnnFeats[:self.on_len]
		sym.rnnFeats = sym.rnnFeats.reshape((1,len(sym.rnnFeats),9))
		diff = self.off_len - len(sym.FKIfeats)
		if diff > 0:
			addit = 100*np.ones(9*diff).reshape((diff,9))
			sym.FKIfeats = np.vstack((sym.FKIfeats, addit))
		else:
			sym.FKIfeats = sym.FKIfeats[:self.off_len]
		sym.FKIfeats = sym.FKIfeats.reshape((1,len(sym.FKIfeats),9))





		prediction = self.SymCLF.predict([sym.rnnFeats,sym.FKIfeats])[0]
		segProb = self.segClf.getProb(sym)
		self.time += time.time() - ti
		pred = prediction.argsort()[::-1][:self.N]
		probTruths = [None]*self.N
		truths = []
		for i in range(self.N):
			truth = self.symK2t[pred[i]]
			if not truth in self.numStroke.keys():
				continue
			probTruths[i] = {'truth':truth,'probability':prediction[pred[i]],'segProb':segProb,'durationProb':self.numStroke[self.symK2t[pred[i]]][len(sym.strokes)],'key':pred[i]}

		if None in probTruths:
			probTruths.remove(None)


		probTruths.sort(key = lambda x: x['probability'],reverse=True)
		sym.symTruthProbs = probTruths
		

		sym.resetCoords()



	def getType(self, truth):

		return self.types[truth]










class RelCLF(object):
	json_file = open('pickled_info/rel_clf/model.json', 'r')
	loaded_model_json = json_file.read()
	json_file.close()
	RelCLF = model_from_json(loaded_model_json)
	RelCLF.load_weights("pickled_info/rel_clf/weights.h5")
	relT2k = {'A':0,'Sup':1,'R':2,'Sub':3,'B':4,'Root':5,'I':6}
	relK2t = {}
	clusterF = 0.15680604
	for k,v in relT2k.items():
		relK2t[v] = k
	def __init__(self):
		self.t = 0
		self.idToFeats = {}

		self.did = set()
		self.m = None
		return

	def getRelProbs(self,c1,c2):
		tt = time.time()

		fro_sym = Symbol()
		to_sym = Symbol()

		for sym in c1.sym:
			for stroke in sym.strokes:
				fro_sym.add_stroke(stroke)

		for sym in c2.sym:
			for stroke in sym.strokes:
				to_sym.add_stroke(stroke)

		fro_sym.name = str(tuple((np.argwhere(c1.ccc).flatten().tolist())))
		to_sym.name = str(tuple((np.argwhere(c2.ccc).flatten().tolist())))

		if not fro_sym.name in self.did:
			self.did.add(fro_sym.name)
			self.idToFeats[fro_sym.name] = {}
		if not to_sym.name in self.did:
			self.did.add(to_sym.name)
			self.idToFeats[to_sym.name] = {}

		if fro_sym.name + to_sym.name in self.did:
			self.t += time.time() - tt
			return self.idToFeats[fro_sym.name][to_sym.name]
		
		feats = fro_sym.relFeats(to_sym)
		prediction = self.RelCLF.predict(np.array([feats]))[0]
		
		probTruths = {}
		for i in range(len(prediction)):
			probTruths[self.relK2t[i]] = prediction[i]

		self.groupPenalty(fro_sym, to_sym, probTruths)

		if to_sym.xmin < fro_sym.xmin:
			probTruths['R'] = 0
			probTruths['Sub'] = 0
			probTruths['Sup'] = 0


		if to_sym.xmax < fro_sym.xmin or to_sym.xmin > fro_sym.xmax:
			probTruths['A'] = 0
			probTruths['B'] = 0			
			probTruths['I'] = 0


		if (fro_sym.xmin < to_sym.xmax < fro_sym.xmax and fro_sym.xmin < to_sym.xmin < fro_sym.xmax) or (to_sym.xmin < fro_sym.xmax < to_sym.xmax and to_sym.xmin < fro_sym.xmin < to_sym.xmax):
			if to_sym.ymin < fro_sym.ymin and to_sym.ymax > fro_sym.ymax:
				probTruths['R'] = 0


		if (fro_sym.ymin > to_sym.ymax or to_sym.ymin > fro_sym.ymax):
			probTruths['R'] = 0
			probTruths['I'] = 0


		if self.solape(fro_sym,to_sym) < 0.5:
			probTruths['I'] = 0










		self.idToFeats[fro_sym.name][to_sym.name] = probTruths
		self.did.add(fro_sym.name + to_sym.name)

		self.t += time.time() - tt
		return probTruths




	def groupPenalty(self, fro_sym, to_sym, probTruths):

		for k in probTruths.keys():
			if k == 'R':
				x1 = fro_sym.xmax
				y1 = fro_sym.ymed
				x2 = to_sym.xmin
				y2 = to_sym.ymed


			if k == 'Sup':
				x1 = fro_sym.xmax
				y1 = fro_sym.ymax
				x2 = to_sym.xmin
				y2 = to_sym.ymin



			if k == 'Sub':
				x1 = fro_sym.xmax
				y1 = fro_sym.ymin
				x2 = to_sym.xmin
				y2 = to_sym.ymax



			if k == 'I':
				x1 = fro_sym.xmin
				y1 = fro_sym.ymed
				x2 = to_sym.xmin
				y2 = to_sym.ymed


			if k == 'A':
				x1 = fro_sym.xmed
				y1 = fro_sym.ymax
				x2 = to_sym.xmed
				y2 = to_sym.ymin

			if k == 'B':
				x1 = fro_sym.xmed
				y1 = fro_sym.ymin
				x2 = to_sym.xmed
				y2 = to_sym.ymax

			d = np.sqrt((x2-x1)**2 + (y2-y1)**2)
			pen = 1/(1+d)
			pen = np.power(pen, self.clusterF)
			probTruths[k] *= pen





	def solape(self, a, b):
		x = max(a.xmin, b.xmin)
		y = max(a.ymin, b.ymin)
		s = min(a.xmax, b.xmax)
		t = min(a.ymax, b.ymax)


		if s >= x and t >= y:

			asolap = (s-x)*(t-y)
			atotal = b.width*b.height
			return asolap/atotal

		return 0





	