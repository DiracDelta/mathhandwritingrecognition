import sys
sys.path.append('..')
from stroke import Stroke
from _symbol import Symbol
from .sample import *
import numpy as np
from .classifiers import SymCLF as symclf
from .classifiers import RelCLF as relclf
from .cellcyk import *
from .grammar import *
from .tablecyk import *
from .logspace import *
from . import hypothesis
import itertools

class meParser(object):
	
	
	clusterF = 0.15680604
	segmentsTH = 0.69474973
	max_strokes = 4
	ptfactor = 0.37846575
	pbfactor = 0.14864657
	qfactor = 1.88593577
	dfactor = 0.93889491
	gfactor = 2.77746769
	rfactor = 0.63225349
	InsPen = 2.11917745
	NB = 3
	


	def __init__(self):
		self.print = True
		self.SymCLF = symclf()
		self.RelCLF = relclf()
		self.G = Grammar()
		return



	def initCYKTerms(self, m, tcyk, N, K):
		#CYK table initialization with the terminal symbols
		self.RelCLF.m = m
		skipNext = False
		for i in range(len(m.strokes)):
			if skipNext:
				skipNext = False
				continue 
			cmy,asc,des = 0,0,0
			if self.print:
				print("Stroke %d:" % i)

			Sym = m.symbols1[str(i)]

			if Sym.possibleX:
				skipNext = True

			cd = CellCYK(len(self.G.NoTerminal),N)

			m.setRegion(cd, [i])

			p = False
			if i == 2:
				p = True


			insert = False

			for prod in self.G.productionTerminals:

				for k in range(self.NB):
					if Sym.symTruthProbs[k]['probability'] > 0 and Sym.symTruthProbs[k]['truth'] in prod.keys and prod.getPrior(Sym.symTruthProbs[k]['truth']) > -1e8:
						prob = np.log(self.InsPen)
						prob += self.ptfactor * prod.getPrior(Sym.symTruthProbs[k]['truth'])
						prob += self.qfactor * np.log(Sym.symTruthProbs[k]['probability']) 
						prob += self.dfactor * np.log(Sym.symTruthProbs[k]['durationProb'])

						if cd.noTerm[prod.getNoTerm()]:
							if cd.noTerm[prod.getNoTerm()].pr > prob + prod.getPrior(Sym.symTruthProbs[k]['truth']):
								continue
							else:
								cd.noTerm[prod.getNoTerm()] = None
								cd.noTermExist.remove(prod.getNoTerm())
						insert = True

						cd.addNoTerm(prod.getNoTerm(), Hypothesis(Sym.symTruthProbs[k]['truth'], prob, cd, prod.getNoTerm()))
						cd.noTerm[prod.getNoTerm()].pt = prod



						typ = self.SymCLF.getType(Sym.symTruthProbs[k]['truth'])
						Sym.setType(typ)
						Sym.setRefPoint()
						cen = Sym.refPoint


						cd.noTerm[prod.getNoTerm()].lcen = cen
						cd.noTerm[prod.getNoTerm()].rcen = cen
						Sym.ccc = cd.ccc


			if insert:
				if self.print:
					for j in range(K):
						if cd.noTerm[j]:
							print("\t%s [%s] %f" % (cd.noTerm[j].class_,self.G.NoTerminal[j], np.exp(cd.noTerm[j].pr)))
					fd = []
					for i in range(len(Sym.symTruthProbs)):
						fd.append(Sym.symTruthProbs[i]['truth'])
					print(fd, len(Sym.strokes))
				
				cd.sym = [Sym]
				tcyk.add(len(Sym.strokes), cd, -1, self.G.isinit)
				# if Sym.possibleX:
				# 	cd.ccc[Sym.ccc] = True

			else:
				del cd




	def combineStrokes(self, M, tcyk, LSP, N):
		gg = {}
		for i in range(10):
			gg[i] = 0
		self.did = {}

		if N <= 1:
			return
		ntested = 0


		distance_th = self.segmentsTH
		M.distance_th = self.segmentsTH

		for i in range(1, len(M.strokeOrder)):

			c1 = CellCYK(len(self.G.NoTerminal), N)
			M.setRegion(c1, [i])

			for size in range(2, self.max_strokes + 1):

				if size == 2:
					close_list = []
					for j in range(i):
						if M.getDist(i,j) < distance_th:
							close_list.append(j)


				else:
					close_list = M.getCloseStrokes(i, distance_th)



				if len(close_list) < size - 1:
					continue

				stkvec = close_list[:]
				stkvec.sort(key = lambda x: int(x))



				VS = len(stkvec)

				for j in range(size-2, VS):


					stks_list = []
					stks_list.append(i)
					stks_list.append(stkvec[j])

					for k in range(j-size+2, j):

						stks_list.append(k)

					stks_list = list(set(stks_list))
					stks_list.sort(key = lambda x: int(x))
					if tuple(stks_list) in self.did.keys():
						continue

					else:
						if not M.isLegal(list(map(str,stks_list))):
							continue
						Sym = Symbol()
						Sym.name = str(tuple(stks_list))
						for id_ in stks_list:
							Sym.add_stroke(M.strokes[str(id_)])
						self.SymCLF.getSymProbs(Sym)
						self.did[tuple(stks_list)] = Sym


					cd = CellCYK(len(self.G.NoTerminal),N)

					M.setRegion(cd, stks_list)
					Sym.ccc = cd.ccc
					if self.print:
						print("Multistroke Hypothesis (%d) Strokes: (%s)" % (size, stks_list))



					ntested += 1


					insert = False

					seg_prob = Sym.symTruthProbs[0]['segProb']


					for prod in self.G.productionTerminals:

						for k in range(self.NB):
							if Sym.symTruthProbs[k]['probability'] > 0.0 and Sym.symTruthProbs[k]['segProb'] > 0.0 and Sym.symTruthProbs[k]['truth'] in prod.keys and prod.getPrior(Sym.symTruthProbs[k]['truth']) > -1e50:
								prob = np.log(self.InsPen)
								prob += self.ptfactor * prod.getPrior(Sym.symTruthProbs[k]['truth'])
								prob += self.qfactor * np.log(Sym.symTruthProbs[k]['probability']) 
								prob += self.dfactor * np.log(Sym.symTruthProbs[k]['durationProb']) 
								prob += self.gfactor * np.log(seg_prob)
								if cd.noTerm[prod.getNoTerm()]:
									if cd.noTerm[prod.getNoTerm()].pr > prob:
										continue

									else:
										cd.noTerm[prod.getNoTerm()] = None
										cd.noTermExist.remove(prod.getNoTerm())


								insert = True
								cd.addNoTerm(prod.getNoTerm(), Hypothesis(Sym.symTruthProbs[k]['truth'], prob, cd, prod.getNoTerm()))
								cd.noTerm[prod.getNoTerm()].pt = prod



								Sym.setRefPoint()
								cen = Sym.refPoint


								cd.noTerm[prod.getNoTerm()].lcen = cen
								cd.noTerm[prod.getNoTerm()].rcen = cen


					if insert:
						if self.print:
							for k in range(cd.nnt):
								if cd.noTerm[k]:
									print("\t%s [%s] %f" % (cd.noTerm[k].class_,self.G.NoTerminal[k], np.exp(cd.noTerm[k].pr)))
							print('\n')

						typ = self.SymCLF.getType(Sym.symTruthProbs[0]['truth'])
						Sym.setType(typ)
						cd.sym = [Sym]
						tcyk.add(size, cd, -1, self.G.isinit)
						

					else:
						continue
		if self.print:
			print('\nPREPARSE TABLE SIZES')
			for size in range(len(M.strokeOrder)):
				print(size, tcyk.size(size))
			print('DONE')






	def fusion(self, M, pd, A, B, N, prob):
		#Combine hypotheses A and B to create new hypothesis S using production 'S -> A B'

		S = None


		if (not A.parent.compatible(B.parent)) or pd.prior < -1e10:
			return S

		grpen = M.groupPenalty(A.parent, B.parent)
		
		
		
		if grpen >= 1e10:
			return None


		grpen = 1/(1 + grpen)

		grpen = np.power(grpen, self.clusterF)

		ps = pd.S
		S = CellCYK(len(self.G.NoTerminal),N)
		#prob = self.pbfactor * pd.prior + self.rfactor + np.log(prob * grpen) + A.pr + B.pr
		prob = self.pbfactor * pd.prior + self.rfactor + np.log(prob) + A.pr + B.pr
		S.xmin = min(A.parent.xmin, B.parent.xmin)
		S.ymin = min(A.parent.ymin, B.parent.ymin)
		S.xmax = max(A.parent.xmax, B.parent.xmax)
		S.ymax = max(A.parent.ymax, B.parent.ymax)



		S.ccUnion(A.parent, B.parent)
		try:
			class_ = self.SymCLF.symT2k[pd.outStr]
		except:
			class_=-1

		S.addNoTerm(ps, Hypothesis(class_, prob, S, ps))
		S.fused = True

		pd.mergeRegions(A,B, S.noTerm[ps])

		S.noTerm[ps].hi = A
		S.noTerm[ps].hd = B
		S.noTerm[ps].prod = pd
		S.sym = A.parent.sym + B.parent.sym


		if int(class_) >= 0:
			for prod in self.G.productionTerminals:
				if class_ in prod.keys() and prod.getPrior(class_) > -1e10:
					S.noTerm[ps].pt = prod
					break
		
		return S



	def rightMost(self, syms):
		return max(syms,key = lambda x: x.xmax)



	def leftMost(self, syms):
		return min(syms,key = lambda x: x.xmin)


	def test_stuff(self, M, sym):
		M.getRefSymbol()
		N = len(M.strokeOrder)
		K = len(self.G.NoTerminal)
		tcyk = TableCYK(N, K)
		logspace = LogSpace(tcyk.get(1), tcyk.size(1), M.RX, M.RY)

		return logspace.getH(sym, ret=True)



	def sudo(self, M):
		M.getRefSymbol()
		N = len(M.strokeOrder)
		K = len(self.G.NoTerminal)
		tcyk = TableCYK(N, K)
		#self.initCYKTerms(M, tcyk, N, K)
		logspace = [None]*N
		logspace[1] = LogSpace(tcyk.get(1), tcyk.size(1), M.RX, M.RY)
		self.combineStrokes(M, tcyk, logspace, N)
		return self.did

	def parse(self, M, r=False):
		self.t = 0
		self.ph = 0
		self.ps = 0
		

		M.getRefSymbol()


		N = len(M.strokeOrder)
		K = len(self.G.NoTerminal)


		tcyk = TableCYK(N, K)

		print('CYK table initialization:')


		self.initCYKTerms(M, tcyk, N, K)

		logspace = [None]*(N+1)


		logspace[1] = LogSpace(tcyk.get(1), tcyk.size(1), M.RX, M.RY)


		self.combineStrokes(M, tcyk, logspace, N)
		print('CYK Parsing Algorithm')



		print("Size 1 Generated: %i" % tcyk.size(1))

		print('\n\t','BEGIN PARSE')




		
		for size in range(2, N + 1):

			for a in range(1, size):

				b = size - a
				c1 = tcyk.get(a)
				
				while c1 != None:

					c1setH = logspace[b].getH(c1)
					c1setV = logspace[b].getV(c1)
					c1setU = logspace[b].getU(c1)
					c1setI = logspace[b].getI(c1)
					c1setM = logspace[b].getM(c1)
					
					for c2 in c1setH:

						if not c1.compatible(c2):
							continue
						tt = time.time()
						for a1,b1 in itertools.product(c1.noTermExist,c2.noTermExist):
							for prod in self.G.pDict[a1][b1]['H']:
								ps = prod.S
								pa = prod.A
								pb = prod.B
								cdpr = self.RelCLF.getRelProbs(c1,c2)['R']
								if cdpr <= 0:
									continue
								cd = self.fusion(M, prod, c1.noTerm[pa], c2.noTerm[pb], len(M.strokeOrder), cdpr)
								if not cd:
									continue


								if cd.noTerm[ps]:
									tcyk.add(size, cd, ps, self.G.isinit)

								else:
									tcyk.add(size, cd, -1, self.G.isinit)






							for prod in self.G.pDict[a1][b1]['Sup']:
								ps = prod.S
								pa = prod.A
								pb = prod.B
								cdpr = self.RelCLF.getRelProbs(c1,c2)['Sup']
								if cdpr <= 0:
									continue
								cd = self.fusion(M, prod, c1.noTerm[pa], c2.noTerm[pb], len(M.strokeOrder), cdpr)
								if not cd:
									continue


								if cd.noTerm[ps]:
									tcyk.add(size, cd, ps, self.G.isinit)

								else:
									tcyk.add(size, cd, -1, self.G.isinit)



							for prod in self.G.pDict[a1][b1]['Sub']:
								ps = prod.S
								pa = prod.A
								pb = prod.B
								cdpr = self.RelCLF.getRelProbs(c1,c2)['Sub']
								if cdpr <= 0:
									continue
								cd = self.fusion(M, prod, c1.noTerm[pa], c2.noTerm[pb], len(M.strokeOrder), cdpr)
								if not cd:
									continue


								if cd.noTerm[ps]:
									tcyk.add(size, cd, ps, self.G.isinit)

								else:
									tcyk.add(size, cd, -1, self.G.isinit)



					for c2 in c1setV:
						if not c1.compatible(c2):
							continue
						tt = time.time()
						for a1,b1 in itertools.product(c1.noTermExist,c2.noTermExist):



							for prod in self.G.pDict[a1][b1]['V']:
								ps = prod.S
								pa = prod.A
								pb = prod.B
								cdpr = self.RelCLF.getRelProbs(c1,c2)['A']
								if cdpr <= 0:
									continue
								cd = self.fusion(M, prod, c1.noTerm[pa], c2.noTerm[pb], len(M.strokeOrder), cdpr)
								if not cd:
									continue


								if cd.noTerm[ps]:
									tcyk.add(size, cd, ps, self.G.isinit)

								else:
									tcyk.add(size, cd, -1, self.G.isinit)




							for prod in self.G.pDict[a1][b1]['Ve']:
								ps = prod.S
								pa = prod.A
								pb = prod.B
								cdpr = self.RelCLF.getRelProbs(c1,c2)['A']
								if cdpr <= 0:
									continue
								cd = self.fusion(M, prod, c1.noTerm[pa], c2.noTerm[pb], len(M.strokeOrder), cdpr)
								if not cd:
									continue


								if cd.noTerm[ps]:
									tcyk.add(size, cd, ps, self.G.isinit)

								else:
									tcyk.add(size, cd, -1, self.G.isinit)

					for c2 in c1setU:
						if not c1.compatible(c2):
							continue
						tt = time.time()
						for a1,b1 in itertools.product(c1.noTermExist,c2.noTermExist):



							for prod in self.G.pDict[a1][b1]['V']:
								ps = prod.S
								pa = prod.A
								pb = prod.B
								cdpr = self.RelCLF.getRelProbs(c1,c2)['B']
								if cdpr <= 0:
									continue
								cd = self.fusion(M, prod, c1.noTerm[pa], c2.noTerm[pb], len(M.strokeOrder), cdpr)
								if not cd:
									continue


								if cd.noTerm[ps]:
									tcyk.add(size, cd, ps, self.G.isinit)

								else:
									tcyk.add(size, cd, -1, self.G.isinit)




							for prod in self.G.pDict[a1][b1]['Ve']:
								ps = prod.S
								pa = prod.A
								pb = prod.B
								cdpr = self.RelCLF.getRelProbs(c1,c2)['B']
								if cdpr <= 0:
									continue
								cd = self.fusion(M, prod, c1.noTerm[pa], c2.noTerm[pb], len(M.strokeOrder), cdpr)
								if not cd:
									continue


								if cd.noTerm[ps]:
									tcyk.add(size, cd, ps, self.G.isinit)

								else:
									tcyk.add(size, cd, -1, self.G.isinit)




					for c2 in c1setI:
						if not c1.compatible(c2):
							continue
						tt = time.time()
						for a1,b1 in itertools.product(c1.noTermExist,c2.noTermExist):
							for prod in self.G.pDict[a1][b1]['Ins']:
								ps = prod.S
								pa = prod.A
								pb = prod.B
								cdpr = self.RelCLF.getRelProbs(c1,c2)['I']
								if cdpr <= 0:
									continue
								cd = self.fusion(M, prod, c1.noTerm[pa], c2.noTerm[pb], len(M.strokeOrder), cdpr)
								if not cd:
									continue


								if cd.noTerm[ps]:
									tcyk.add(size, cd, ps, self.G.isinit)

								else:
									tcyk.add(size, cd, -1, self.G.isinit)

						self.t += time.time() - tt





					for c2 in c1setM:
						if not c1.compatible(c2):
							continue
						tt = time.time()
						for a1,b1 in itertools.product(c1.noTermExist,c2.noTermExist):



							for prod in self.G.pDict[a1][b1]['Mrt']:
								ps = prod.S
								pa = prod.A
								pb = prod.B
								cdpr = self.RelCLF.getRelProbs(c1,c2)['Root']
								if cdpr <= 0:
									continue
								cd = self.fusion(M, prod, c1.noTerm[pa], c2.noTerm[pb], len(M.strokeOrder), cdpr)
								if cd == None:
									continue


								if cd.noTerm[ps]:
									tcyk.add(size, cd, ps, self.G.isinit)

								else:
									tcyk.add(size, cd, -1, self.G.isinit)

						self.t += time.time() - tt





					for pps in range(c1.nnt):
						if c1.noTerm[pps] and c1.noTerm[pps].prod and c1.noTerm[pps].prod.type == 'B':

							c1setS = logspace[b + c1.noTerm[pps].hi.parent.size].getS(c1)

							for c2 in c1setS:

								if not c1.compatible(c2):
									continue

								tt = time.time()

								for a1,b1 in itertools.product(c1.noTermExist,c2.noTermExist):

									for prod in self.G.pDict[a1][b1]['SSE']:
										ps = prod.S
										pa = prod.A
										pb = prod.B


										if c1.noTerm[pa].hi == c2.noTerm[pb].hi and c1.noterm[pa].prod.type == 'B' and c2.noterm[pb].prod.type == 'P' and c1.noTerm[pa].hd.parent.compatible(c2.noTerm[pb].hd.parent):

											if abs(c1.noTerm[pa].hd.parent.xmin - c1.noTerm[pb].hd.parent.xmin) > 3*M.RX:
												continue

											if max(prod.solape(c1.noTerm[pa].hd, c2.noTerm[pb].hd), prod.solape(c2.noTerm[pb].hd, c1.noTerm[pa].hd)) > 0.1:
												continue


											prob = c1.noTerm[pa].pr + c2.noTerm[pb].pr - c1.noTerm[pa].hi.pr


											cd = CellCYK(len(G.NoTerminal), len(M.strokeOrder))

											cd.xmin = min(c1.xmin, c2.xmin)
											cd.ymin = min(c1.ymin, c2.ymin)
											cd.xmax = max(c1.xmax, c2.xmax)
											cd.ymax = max(c1.ymax, c2.ymax)



											cd.addNoTerm(ps, Hypothesis(-1, prob, cd, ps))


											cd.noTerm[ps].lcen = c1.noTerm[pa].lcen
											cd.noTerm[ps].rcen = c1.noTerm[pa].rcen

											cd.noTerm[ps].hi = c1.noTerm[pa]
											cd.noTerm[ps].hd = c2.noTerm[pb].hd
											cd.noTerm[ps].prod = prod
											cd.noterm[ps].prod_sse = c2.noterm[pb].prod

											tcyk.add(size, cd, ps, self.G.isinit)










					c1 = c1.sig










					



			if size < N:

				logspace[size] = LogSpace(tcyk.get(size), tcyk.size(size), M.RX, M.RY)




			print("Size %i Generated: %s" % (size, tcyk.size(size)))





		mlh = tcyk.getMLH()
		print("Most Likely Hypothesis: %i strokes" % mlh.parent.size)
		self.printSymrec(mlh)
		return self.printLatex(mlh, r)
		print(self.t)








	def printSymrec(self, H):

		if not H.pt:
			self.printSymrec(H.hi)
			self.printSymrec(H.hd)


		else:
			print(H.pt.tex,np.argwhere(H.parent.ccc).flatten())




	def printLatex(self, H, r):

		if not H.pt:

			if r:
				print(H.prod.getOut(H))
				return H.prod.getOut(H)
			else:
				print(H.prod.getOut(H))

		else:
			if r:
				print(H.class_)
				return H.class_
			else:
				print(H.class_)	



