import sys
sys.path.append('..')
from stroke import Stroke
from _symbol import Symbol




import numpy as np




class LogSpace(object):

	def __init__(self, cell, nr, dx, dy):




		self.N = nr
		self.RX = dx
		self.RY = dy


		self.data = []
		if cell == None:
			return

		c = cell
		self.data.append(c)
		i = 0
		while c.sig != None:

			c = c.sig
			i += 1
			self.data.append(c)


		self.data.sort(key=lambda x: x.xmin)






	def getH(self, c, ret=False):

		#  (xmin,ymax)------
		#   ------------
		#   ------------
		#   ------(xmax,ymin)

		xmin = max(c.xmin, c.xmax - 2*self.RX)
		xmax = c.xmax + self.RX * 8
		ymin = c.ymin - 1*self.RY
		ymax = c.ymax + 1*self.RY

		if ret:
			return (xmin,xmax,ymin,ymax)


		return self.bsearchHPB(xmin, ymin, xmax, ymax, c)



	def getV(self, c, ret=False):
	#  Search below region (fraction denominator)
		xmin = c.xmin - 2*self.RX
		xmax = c.xmax + 2*self.RX
		ymin = max(c.ymax - self.RY, c.ymin)
		ymax = c.ymin + self.RY*3
		if ret:
			return (xmin,xmax,ymin,ymax)

		return self.bsearchStv(xmin, ymin, xmax, ymax, False, c)




	def getU(self, c, ret=False):
    #Above region. Although only Below is really considered this is necessary to
    #solve the problem of the case | aaa|
    #                              |bbbb|
    #such that "a" would never find "b" because its 'sx' would start before "b.x"
		xmin = c.xmin - 2*self.RX
		xmax = c.xmax + 2*self.RX
		ymin = c.ymin -3*self.RY
		ymax = min(c.ymin + self.RY, c.ymax)
		if ret:
			return (xmin,xmax,ymin,ymax)

		return self.bsearchStv(xmin, ymin, xmax, ymax, True, c)




	def getI(self, c, ret=False):
	# Search in inside region (sqrt)
		xmin = c.xmin
		xmax = c.xmax + self.RX
		ymin = c.ymin
		ymax = c.ymax + self.RY

		if ret:
			return (xmin,xmax,ymin,ymax)
		return self.bsearch(xmin, ymin, xmax, ymax)



	def getM(self, c, ret = False):
		# Mroot region (n-th sqrt)
		xmin = c.xmin - 2*self.RX
		xmax = min(c.xmin + 2*self.RX, c.xmax)
		ymin = c.ymin - self.RY
		ymax = min(c.ymin + 2*self.RY, c.ymax)
		if ret:
			return (xmin,xmax,ymin,ymax)

		return self.bsearch(xmin, ymin, xmax, ymax)





	def getS(self, c, ret=False):
        #(sx,sy)------
        #------------
        #------------
        #------(ss,st)
		xmin = c.xmin
		xmax = c.xmax
		ymin = c.ymin - self.RY
		ymax = c.ymax + self.RY
		if ret:
			return (xmin,xmax,ymin,ymax)

		return self.bsearch(xmin, ymin, xmax, ymax)













	def bsearch(self, xmin, ymin, xmax, ymax):
		#Binary search of "sx"
		inreg = []

		i = 0
		j = self.N

		while i < j:
			m = (i + j)//2
			if xmin <= self.data[m].xmin:
				j = m
			else:
				i = m + 1

		while i < self.N and self.data[i].xmin <= xmax:
			if self.data[i].ymin <= ymax and self.data[i].ymax >= ymin:
				inreg.append(self.data[i])

			i += 1

		return inreg



	def bsearchStv(self, xmin, ymin, xmax, ymax, U_V, cd):
		#Version more strict with the upper/lower vertical positions
		inreg = []

		i = 0
		j = self.N

		while i < j:
			m = (i + j)//2
			if xmin <= self.data[m].xmin:
				j = m
			else:
				i = m + 1



		if U_V:
			while i < self.N and self.data[i].xmin <= xmax:
				if self.data[i].ymax <= ymax and self.data[i].ymax >= ymin and self.data[i].xmax <= xmax:
					if self.data[i].ymax < cd.ymin:
						ymin = max(max(self.data[i].ymin, self.data[i].ymax - self.RY), ymin)


					inreg.append(self.data[i])


				i += 1

		else:

			while i < self.N and self.data[i].xmin <= xmax:
				if self.data[i].ymin <= ymax and self.data[i].ymin >= ymin and self.data[i].xmax <= xmax:
					if self.data[i].ymin > cd.ymax:
						ymax = min(min(self.data[i].ymax, self.data[i].ymin + self.RY), ymax)


					inreg.append(self.data[i])

				i += 1


		return inreg




	def bsearchHPB(self, xmin, ymin, xmax, ymax, cd):
		#Version that reduces the sx-ss region as soon as hypotheses are found
		inreg = []

		i = 0
		j = self.N
		while i < j:
			m = (i + j)//2
			if xmin <= self.data[m].xmin:
				j = m
			else:
				i = m + 1
		while i < self.N and self.data[i].xmin <= xmax:
			if self.data[i].ymin <= ymax and self.data[i].ymax >= ymin and cd.compatible(self.data[i]):
				if self.data[i].xmin > cd.xmax:
					xmax = min(min(self.data[i].xmax, self.data[i].xmin + self.RX), xmax)

				inreg.append(self.data[i])

			i += 1
		return inreg




