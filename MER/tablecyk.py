import sys
sys.path.append('..')
from stroke import Stroke
from _symbol import Symbol
import numpy as np
from .grammar import *
import re
from .production import *
import pickle
from .cellcyk import *
from .hypothesis import *
import time



class coo(object):

	num = 0


	def __init__(self,a=-1,b=-1,c=-1,d=-1):
		self.xmin = a
		self.ymin = b
		self.xmax = c
		self.ymax = d
		self.NN = self.num
		self.num += 1
		self.cell = None


	def __eq__(self, R):
		return self.xmin == R.xmin and self.ymin == R.ymin and self.xmax == R.xmax and self.ymax == R.ymax


	def __lt__(self, C):
		if self.xmin < C.xmin:
			return True

		if self.xmin == C.xmin:
			if self.ymin < C.ymin:
				return True
			if self.ymin == C.ymin:
				if self.xmax < C.xmax:
					return True

				if self.xmax == C.xmax:
					if self.ymax < C.ymax:
						return True
		return False



class TableCYK(object):
#Cocke-Younger-Kasami (CYK) algorithm for 2D-SCFG

	def __init__(self,n,k):
		self.t = 0
		self.n = n
		self.k = k

		self.T = [None]*self.n
		self.Target = Hypothesis(-1, -1e50, None, -1)
		self.Target.shit = False
		self.pm_comps = 0
		self.TS = [{} for x in range(self.n)]


	def getMLH(self):
		return self.Target


	def get(self, n):
		return self.T[n-1]

	def size(self, n):
		if not self.TS[n - 1]:
			return 0
		return len(self.TS[n-1])




	def updateTarget(self, K, H):
		pcomps = H.parent.ccc.sum()
		if pcomps > self.pm_comps or (pcomps == self.pm_comps and H.pr > self.Target.pr):
			self.Target.copy(H)
			self.pm_comps = pcomps


	def add(self, n, cell, noTerm_id, isinit):
		tt = time.time()
		cell.size = n
		key = coo(cell.xmin,cell.ymin,cell.xmax,cell.ymax)
		k = -1
		for k_,cell2 in self.TS[n-1].items():
			if cell2:
				if key == cell2.coo:
					k = k_
					break


		if k == -1:
			cell.sig = self.T[n-1]
			self.T[n-1] = cell
			self.T[n-1].coo = key
			key.cell = self.T[n-1]
			kkk = len(self.TS[n-1])
			self.TS[n-1][kkk] = cell

			if noTerm_id >= 0:
				if isinit[noTerm_id]:
					cell.noTerm[noTerm_id].parent = cell
					self.updateTarget(key, cell.noTerm[noTerm_id])

			else:
				for i in range(cell.nnt):
					if cell.noTerm[i] and isinit[i]:
						cell.noTerm[i].parent = cell
						self.updateTarget(key, cell.noTerm[i])

		else:
			Va = 0
			Vb = 0

			if noTerm_id < 0:
				Va = 0
				Vb = cell.nnt

			else:
				Va = noTerm_id
				Vb = Va + 1

			r = self.TS[n-1][k]

			if not cell.ccEqual(r):
				maxpr_c = -1e50

				for i in range(Va,Vb):
					if cell.noTerm[i] and cell.noTerm[i].pr > maxpr_c:
						maxpr_c = cell.noTerm[i].pr

				maxpr_r = -1e50
				for i in range(cell.nnt):
					if r.noTerm[i] and r.noTerm[i].pr > maxpr_r:
						maxpr_r = r.noTerm[i].pr



				if maxpr_c > maxpr_r:
					r.ccc = cell.ccc

					for i in range(cell.nnt):
						if cell.noTerm[i]:
							if r.noTerm[i]:
								r.noTerm[i].copy(cell.noTerm[i])
								r.noTerm[i].parent = r

								if isinit[i]:
									r.noTerm[i].parent = r
									self.updateTarget(key, r.noTerm[i])

							else:
								r.noTerm[i] = cell.noTerm[i]
								r.noTermExist.add(i)
								r.noTerm[i].parent = r
								cell.noTerm[i] = None
								cell.noTermExist.remove(i)

								if isinit[i]:
									r.noTerm[i].parent = r
									self.updateTarget(key, r.noTerm[i])

						elif r.noTerm[i]:
							r.noTerm[i] = None
							r.noTermExist.remove(i)

				del cell
				self.t += time.time() - tt
				return


			for i in range(Va,Vb):
				if cell.noTerm[i]:
					if r.noTerm[i]:
						if cell.noTerm[i].pr > r.noTerm[i].pr:
							r.noTerm[i].copy(cell.noTerm[i])
							r.noTerm[i].parent = r

							if isinit[i]:
								self.updateTarget(key, r.noTerm[i])

					else:
						r.noTerm[i] = cell.noTerm[i]
						r.noTermExist.add(i)
						r.noTerm[i].parent = r


						cell.noTerm[i] = None
						cell.noTermExist.remove(i)

						if isinit[i]:
							r.noTerm[i].parent = r
							self.updateTarget(key, r.noTerm[i])

			del cell
		self.t += time.time() - tt


























