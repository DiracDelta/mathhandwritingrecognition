import sys
sys.path.append('..')
from stroke import Stroke
from _symbol import Symbol
import numpy as np
from .grammar import *
import re
from .production import *
import pickle
from .tablecyk import *

class CellCYK(object):

	def __init__(self, n, ncc):
		self.xmin = None
		self.ymin = None
		self.xmax = None
		self.ymax = None
		self.sig = None
		self.nnt = n
		self.nc = ncc
		self.size = 0
		self.noTerm = [None]*self.nnt
		self.ccc = np.array([False]*self.nc)
		self.fused = False
		self.noTermExist = set()


	def __lt__(self, C):
		# Define less than comparison for log space ordering.
		if self.xmin < C.xmin:
			return True

		if self.xmin == C.xmin:
			if self.ymin < C.ymin:
				return True
			if self.ymin == C.ymin:
				if self.xmax < C.xmax:
					return True

				if self.xmax == C.xmax:
					if self.ymax < C.ymax:
						return True
		return False




	def addNoTerm(self, n, H):
		self.noTermExist.add(n)
		self.noTerm[n] = H



	def ccUnion(self, A, B):
		#Set the covered strokes to the union of cells A and B
		self.ccc = np.logical_or(A.ccc,B.ccc)



	def ccEqual(self, H):
		#Check if cell H covers the same strokes that this
		if self.size != H.size:
			return False

		return (self.ccc == H.ccc).all()



	def compatible(self, H):
		#Check if the intersection between the strokes of this cell and H is empty
		return not np.logical_and(self.ccc,H.ccc).any()





