import plaidml.keras
plaidml.keras.install_backend()
import sys
sys.path.append('..')
from stroke import Stroke
from _symbol import Symbol
from expression import Expression
from classifiers import SymCLF as symclf
from classifiers import RelCLF as relclf
from spans import floatrange, floatrangeset
import numpy as np
import pickle
from .meparser import *
from .logspace import *
from .sample import *
from .hypothesis import *
from .cellcyk import *
from .tablecyk import *
from .production import *
from .grammar import *
import os
import time
sss = symclf()

G = Grammar()

E = pickle.load(open('/Users/boltzmann/Desktop/OAbacus/orig/Abacus/train_expressions/200923-1556-84.pkl','rb'))

for s_id in E.stkorder:
	stroke = E.strokes[s_id]
	stroke.applySmoothing()
	stroke.skipCheck = False
	stroke.noExtra = False
	print(s_id, E.syms[stroke.symbol].truth)
E.setup()
# E.plot_all().show()
# sys.exit()




seshat = meParser()
M = Sample(E.strokes,E.stkorder, seshat)


seshat.parse(M)
E.plot_all().show()