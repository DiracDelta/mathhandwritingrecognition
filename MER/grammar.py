import sys
sys.path.append('..')
from stroke import Stroke
from _symbol import Symbol
from .classifiers import SymCLF as symclf
from .classifiers import RelCLF as relclf
import numpy as np
from .grammar import *
import re
from .production import *
import pickle
from .hypothesis import *

class Grammar:
	# Stores grammar rules.
	symT2k = pickle.load(open('pickled_info/truth_to_key.pkl','rb'))
	symK2t = {}
	for k,v in symT2k.items():
		symK2t[v] = k

	ptermf = 'MER/pterm.gram'
	initf = 'MER/init.gram'
	pbinf = 'MER/pbin.gram'
	notermf = 'MER/noterm.gram'
	def __init__(self):
		self.addRules = {
						'H': self.addRuleH,
						'V': self.addRuleV,
						'Ve': self.addRuleVe,
						'Sub': self.addRuleSub,
						'Sup': self.addRuleSup,
						'SSE': self.addRuleSSE,
						'Ins': self.addRuleIns,
						'Mrt': self.addRuleMrt}
		self.productionTerminals = []
		self.productionH = []
		self.productionV = []
		self.productionVe = []
		self.productionSub = []
		self.productionSup = []
		self.productionSSE = []
		self.productionIns = []
		self.productionMrt = []
		self.line = 0

		self.loadNoTerm()
		self.loadInitSyms()
		self.loadPTerm()
		self.loadPBin()
		self.setupDict()


	def loadNoTerm(self):
		self.NoTerminal = []
		with open(self.notermf,'r') as f:
			lines = f.read().split('\n')

		for line in lines:
			self.NoTerminal.append(line)



	def loadInitSyms(self):

		self.InitSyms = []
		with open(self.initf,'r') as f:
			lines = f.read().split('\n')

		for line in lines:
			self.InitSyms.append(line)

		self.isinit = np.array([False]*len(self.NoTerminal))
		for t in self.InitSyms:
			self.isinit[self.NoTerminal.index(t)] = True


	def loadPBin(self):

		with open(self.pbinf,'r') as f:
			lines = f.read().split('\n')

		for line in lines:
			self.addRule(re.findall(r'".*?"|\S+', line))


	def loadPTerm(self):

		with open(self.ptermf,'r') as f:
			lines = f.read().split('\n')

		for line in lines:
			self.addTerminal(line.split(' '))




	def setupDict(self):
		self.pDict = {}
		for i in range(len(self.NoTerminal)):
			self.pDict[i] = {}
			for j in range(len(self.NoTerminal)):
				self.pDict[i][j] = {'H':[],'Sup':[],'Sub':[],'V':[],'Ve':[],'SSE':[],'Ins':[],'Mrt':[]}
		
		for prod in self.productionH:
			self.pDict[prod.A][prod.B]['H'].append(prod)
		for prod in self.productionSup:
			self.pDict[prod.A][prod.B]['Sup'].append(prod)
		for prod in self.productionSub:
			self.pDict[prod.A][prod.B]['Sub'].append(prod)
		for prod in self.productionV:
			self.pDict[prod.A][prod.B]['V'].append(prod)
		for prod in self.productionVe:
			self.pDict[prod.A][prod.B]['Ve'].append(prod)
		for prod in self.productionSSE:
			self.pDict[prod.A][prod.B]['SSE'].append(prod)
		for prod in self.productionIns:
			self.pDict[prod.A][prod.B]['Ins'].append(prod)
		for prod in self.productionMrt:
			self.pDict[prod.A][prod.B]['Mrt'].append(prod)






	def addTerminal(self, line):
		pr, typ, val, tex = line
		pr = float(pr)
		create = True
		pt = ProductionT(typ)
		pt.G = self
		pt.setClass(pr,tex,'i')
		self.productionTerminals.append(pt)
		return

	def addRule(self, line):
		pr,_,S,A,B,out,merge = line
		pr = float(pr)
		self.addRules[line[1]](pr,S,A,B,out,merge)
		self.line += 1



	def addRuleH(self,pr,S,A,B,out,merge):
		pd = ProductionH(S,A,B,pr,out)
		pd.setMerges(merge)
		pd.G = self
		self.productionH.append(pd)
		pd.line = self.line
		return


	def addRuleV(self,pr,S,A,B,out,merge):
		pd = ProductionV(S,A,B,pr,out)
		pd.setMerges(merge)
		pd.G = self
		self.productionV.append(pd)
		pd.line = self.line
		return

	def addRuleVe(self,pr,S,A,B,out,merge):
		pd = ProductionVe(S,A,B,pr,out)
		pd.setMerges(merge)
		pd.G = self
		self.productionVe.append(pd)
		pd.line = self.line
		return

	def addRuleSup(self,pr,S,A,B,out,merge):
		pd = ProductionSup(S,A,B,pr,out)
		pd.setMerges(merge)
		pd.G = self
		self.productionSup.append(pd)
		pd.line = self.line
		return

	def addRuleSub(self,pr,S,A,B,out,merge):
		pd = ProductionSub(S,A,B,pr,out)
		pd.setMerges(merge)
		pd.G = self
		self.productionSub.append(pd)
		pd.line = self.line
		return

	def addRuleSSE(self,pr,S,A,B,out,merge):
		pd = ProductionSSE(S,A,B,pr,out)
		pd.setMerges(merge)
		pd.G = self
		self.productionSSE.append(pd)
		pd.line = self.line
		return


	def addRuleIns(self,pr,S,A,B,out,merge):
		pd = ProductionIns(S,A,B,pr,out)
		pd.setMerges(merge)
		pd.G = self
		self.productionIns.append(pd)
		pd.line = self.line
		return


	def addRuleMrt(self,pr,S,A,B,out,merge):
		pd = ProductionMrt(S,A,B,pr,out)
		pd.setMerges(merge)
		pd.G = self
		self.productionMrt.append(pd)
		pd.line = self.line
		return