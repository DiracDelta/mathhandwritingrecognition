import sys
sys.path.append('..')
from stroke import Stroke
from _symbol import Symbol
from .grammar import *
import numpy as np
import pickle
from .hypothesis import *





class ProductionT(object):
	symT2k = pickle.load(open('pickled_info/truth_to_key.pkl','rb'))
	symK2t = {}
	for k,v in symT2k.items():
		symK2t[v] = k
	def __init__(self, s):
		self.S = s
		self.keys = set()
		self.info = {}


	def setClass(self, pr, tex, mlt):
		self.tex = tex
		self.keys.add(tex)
		self.info[tex] = {'pr':0,'mlt':mlt}
		return

	def getPrior(self, tex):
		if not tex in self.keys:
			return -np.inf
		else:
			return self.info[tex]['pr']

	def getNoTerm(self):
		return self.G.NoTerminal.index(self.S)




class Production(object):
	symT2k = pickle.load(open('/Users/boltzmann/Desktop/OAbacus/orig/Abacus/pickled_info/truth_to_key.pkl','rb'))
	symK2t = {}
	for k,v in symT2k.items():
		symK2t[v] = k
	NoTerminal = ['2dot','ArrTerm','Arrow','Bar','BigOp','BigOpSub','BigOpSup','BigOpTerm','BigOpV',
				  'BrTerm','CDots','CloseBckt','CloseBra','ClosePar','ComListR','Comma','DBar','DeSeDig',
				  'DecSep','Digit','Dot','Facto','Float','Greek','LDots','LetterCap','LetterMin','Limit',
				  'ListR','Mroot','OpBin','OpDots','OpSum','OpSym','OpTerm','OpUn','OpenBckt','OpenBra',
				  'OpenPar','Prime','Sqrt','SubLimit','Sym','Term','TermB','TermBar','TermBckt','TermBra',
				  'TermDBar','TermP','TermPar','TermSSE','TermSub','TermSup','UnTerm','VFrac','VeFrac','an',
				  'co','hl_dot','hline','im','in','leta','letc','letg','leti','letl','letm','letn','leto','lets',
				  'lett','li','lo','og','os','sgt','si','slt','ta']
	def __init__(self, s, a, b, pr=None, out=None):
		self.S = self.NoTerminal.index(s)
		self.A = self.NoTerminal.index(a)
		self.B = self.NoTerminal.index(b)
		self.outStr = out
		self.prior = 0

		return

	def solape(self, a, b):
		#Percentage of the are of regin A that overlaps with region B
		xmin = max(a.parent.xmin, b.parent.xmin)
		ymin = max(a.parent.ymin, b.parent.ymin)
		xmax = min(a.parent.xmax, b.parent.xmax)
		ymax = min(a.parent.ymax, b.parent.ymax)


		if xmax >= xmin and ymax >= ymin:

			asolap = (xmax-xmin)*(ymax-ymin)
			atotal = (a.parent.xmax - a.parent.xmin)*(a.parent.ymax - a.parent.ymin)
			return asolap/atotal

		return 0

		


	def setMerges(self, c):
		self.merge_cen = c


	def mergeRegions(self, Ha, Hb, Hc):
		if self.merge_cen == 'A':
			Hc.lcen = Ha.lcen
			Hc.rcen = Ha.rcen

		elif self.merge_cen == 'B':
			Hc.lcen = Hb.lcen
			Hc.rcen = Hb.rcen

		elif self.merge_cen == 'C':
			Hc.lcen = (Ha.parent.ymin + Ha.parent.ymax)/2
			Hc.rcen = (Hb.parent.ymin + Hb.parent.ymax)/2

		elif self.merge_cen == 'M':
			Hc.lcen = (Ha.lcen + Hb.lcen)/2
			Hc.rcen = (Ha.rcen + Hb.rcen)/2


	def getOut(self, H):
		
		# Get output in latex

		out = self.outStr.replace('"','')
		a1 = '$1'
		a2 = '$2'			


		if H.hi.class_ == -1:
			t1 = H.hi.prod.getOut(H.hi)

		else:
			t1 = H.hi.class_

		if H.hd.class_ == -1:
			t2 = H.hd.prod.getOut(H.hd)

		else:
			t2 = H.hd.class_





		out = out.replace(a1,t1)
		out = out.replace(a2,t2)
		out = out.replace('COMMA',',')
		out = out.replace('equal','=')
		return out




class ProductionH(Production):
	def __init__(self, s, a, b, pr=None, out=None):
		Production.__init__(self, s, a, b, pr=pr, out=out)
		self.type = 'H'
		return



class ProductionV(Production):
	def __init__(self, s, a, b, pr=None, out=None):
		Production.__init__(self, s, a, b, pr=pr, out=out)
		self.type = 'V'



class ProductionVe(Production):
	def __init__(self, s, a, b, pr=None, out=None):
		Production.__init__(self, s, a, b, pr=pr, out=out)
		self.type = 'e'



class ProductionSup(Production):
	def __init__(self, s, a, b, pr=None, out=None):
		Production.__init__(self, s, a, b, pr=pr, out=out)
		self.type = 'P'



class ProductionSub(Production):
	def __init__(self, s, a, b, pr=None, out=None):
		Production.__init__(self, s, a, b, pr=pr, out=out)
		self.type = 'B'


class ProductionSSE(Production):
	def __init__(self, s, a, b, pr=None, out=None):
		Production.__init__(self, s, a, b, pr=pr, out=out)
		self.type = 'S'



class ProductionIns(Production):
	def __init__(self, s, a, b, pr=None, out=None):
		Production.__init__(self, s, a, b, pr=pr, out=out)
		self.type = 'I'



class ProductionMrt(Production):
	def __init__(self, s, a, b, pr=None, out=None):
		Production.__init__(self, s, a, b, pr=pr, out=out)
		self.type = 'M'











