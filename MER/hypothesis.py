import sys
sys.path.append('..')
from stroke import Stroke
from _symbol import Symbol
import numpy as np




def rightMost(h):
	if h.pt:
		return h

	izq = rightMost(h.hi)
	der = rightMost(h.hd)


	if izq.parent.x < der.parent.x:
		return izq

	else:
		return der

def leftMost(h):
	if h.pt:
		return h

	izq = leftMost(h.hi)
	der = leftMost(h.hd)


	if izq.parent.x > der.parent.x:
		return izq

	else:
		return der




class Hypothesis(object):

	def __init__(self, c, p , cd, nt):
		self.class_    = c
		self.pr       = p
		self.hi       = None
		self.hd       = None
		self.prod     = None
		self.prod_sse = None
		self.pt       = None
		self.parent   = cd
		self.ntid     = nt
		self.typ = None
		return



	def copy(self,other):
		self.class_ = other.class_
		self.pr   = other.pr
		self.hi   = other.hi
		self.hd   = other.hd
		self.prod = other.prod
		self.prod_sse = other.prod_sse
		self.pt   = other.pt
		self.parent = other.parent
		self.ntid = other.ntid
		self.typ = other.typ






