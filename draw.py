from keras.models import model_from_json
from random import random
import copy
from kivy.app import App
import traceback
from kivy.uix.image import *
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.graphics import Color, Ellipse, Line, Rectangle
import numpy as np
import matplotlib.pyplot as plt
from stroke import *
from _symbol import *
import pickle
from MER.meparser import meParser
from MER.sample import Sample
class MyPaintWidget(Widget):

    def on_touch_down(self, touch):
        with self.canvas:
            Color(0, 1, 1)
            touch.ud['line'] = Line(points=(touch.x, touch.y),width=1.5)
            touch.ud['x'] = [touch.x]
            touch.ud['y'] = [touch.y]


    def on_touch_move(self, touch):
        touch.ud['line'].points += [touch.x, touch.y]
        touch.ud['x'].append(touch.x)
        touch.ud['y'].append(touch.y)

    def on_touch_up(self, touch):
        try:
            touch.ud['x']
        except:
            return
        try:
            if len(touch.ud['x']) == 0:
                return
            s = Stroke([np.array(touch.ud['x']),np.array(touch.ud['y'])])
            s.setup()
            s.smoothConv()
            i = str(len(self.strokeOrder))
            s.id = i
            self.strokes[i] = s
            self.strokeOrder.append(i)

        except Exception as e:
            print(traceback.format_exc())
            pass

class MyPaintApp(App):




    def build(self):
        t2kp = 'pickled_info/truth_to_key.pkl'
        self.n2k = pickle.load(open(t2kp,'rb'))
        self.k2n = {}
        self.num = 0
        for key,val in self.n2k.items():
            self.k2n[val] = key
        self.plt = plt
        parent = Widget()
        self.painter = MyPaintWidget()
        self.painter.strokes = {}
        self.painter.strokeOrder = []
        #self.image = Image(source='tmp.jpg',pos=(self.painter.x,self.painter.height))
        clearbtn = Button(text='Clear')
        clearbtn.bind(on_release=self.clear_canvas)
        classify = Button(pos=(self.painter.x, self.painter.height),text='Classify')
        classify.bind(on_release=self.classify_sym)
        self.label = Label(text='',markup=True,pos=(400,400))
        parent.add_widget(self.painter)
        parent.add_widget(clearbtn)
        parent.add_widget(classify)
        parent.add_widget(self.label)
        return parent

    def clear_canvas(self, obj):
        self.painter.strokes = {}
        self.painter.canvas.clear()
        self.painter.strokeOrder = []
        self.label.text = ""

    def classify_sym(self, obj):
        try:
            parser = meParser()
            sample = Sample(self.painter.strokes,self.painter.strokeOrder,parser)
            out = parser.parse(sample,r=True)
            

            self.label.text = out
            del parser
            del sample

        except Exception as e:
            print(traceback.format_exc())

        # try:
        #     seshat = meParser()
        #     sample = Sample(self.painter.strokes,self.painter.strokeOrder,seshat)
        #     xmin,xmax,ymin,ymax = seshat.test_stuff(sample, self.painter.strokes[self.painter.strokeOrder[-1]])
            
        #     for stroke in self.painter.strokes.values():
        #         plt.plot(stroke.x,stroke.y,color='black')

        #     x = [xmin,xmin,xmax,xmax,xmin]
        #     y = [ymax,ymin,ymin,ymax,ymax]
        #     plt.plot(x,y,color='red')
        #     plt.show()

        #     #self.label.text = out
        #     del seshat
        #     del sample

        # except Exception as e:
        #     print(traceback.format_exc())        




if __name__ == '__main__':
    MyPaintApp().run()
