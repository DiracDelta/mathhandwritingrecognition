import numpy as np
from scipy.spatial.distance import cdist
from math import *
from scipy.signal import argrelmax,argrelmin
from scipy.spatial import ConvexHull
#from simplification.cutil import simplify_coords




class BreakLoop(Exception): pass

class Stroke(object):


	def __init__(self,seq):
		#Booleans to prevent unnessecary computation
		self.didlcf = False
		self.didhist = False
		self.didgabor = False
		self.gotHull = False
		self.withAnother = False
		x,y = seq
		self.id = ''
		self.symbol = None
		self.x = x[:]
		self.y = y[:]
		self.sharp_points = None
		self.segments = None
		self.noExtra = False
		self.skipCheck = False
		self.smoothed = False

	def setup(self):
		#Setup important values
		self.xmax = np.max(self.x)
		self.ymax = np.max(self.y)
		self.xmin = np.min(self.x)
		self.ymin = np.min(self.y)
		self.xmed = (self.xmax + self.xmin)/2
		self.ymed = (self.ymax + self.ymin)/2
		self.y25 = (self.ymed + self.ymin)/2
		self.y75 = (self.ymed + self.ymax)/2
		self.x25 = (self.xmed + self.xmin)/2
		self.x75 = (self.xmed + self.xmax)/2
		self.width = self.xmax - self.xmin
		self.height = self.ymax - self.ymin
		self.diag = np.sqrt(self.width**2 + self.height**2)
		self.half_diag = self.diag/2
		self.area = self.width*self.height

		self.getSharpPoints()

		if self.width == 0:
			self.width = 1e-3
		if self.height == 0:
			self.height = 1e-3
		self.aspect = self.width/self.height

		self.length = np.sqrt(((self.x[1:]-self.x[:-1])**2) + (self.y[1:]-self.y[:-1])**2).sum()
		self.fixed = False


	def applySmoothing(self):
		if self.smoothed:
			return
		self.smoothed = True
		for i in range(2):
			self.remove_hooks()
		self.removeDuplicatedPoints()
		if len(self.x) <= 5:
			self.addMissingPoints(epsilon=0.25)
			return

		if self.hasDuplicatePoints():
			self.removeDuplicatedPoints()
		self.setup()
		sharp_points = self.sharpPoints
		self.removeHooks(sharp_points)
		self.splineResample(sharp_points, 2)
		self.simplify()
		if len(self.x) <= 3:
			self.addMissingPoints(epsilon=0.25)
		else:
			self.addMissingPoints(epsilon=0.8)
		self.setup()
		


	def getConvexHull(self):
		if self.gotHull:
			return
		self.gotHull = True
		points = np.dstack((self.x,self.y))[0]
		try:
			hull = ConvexHull(points).simplices[0]
			self.hullx = points[hull, 0]
			self.hully = points[hull, 1]
		except:
			self.hullx = self.x
			self.hully = self.y


	def newSmooth(self,x,y):
		sharp_points = list(zip(x,y))
		sharp_points = list(enumerate(sharp_points))
		self.splineResample(sharp_points, 4)


	def findMaxSpan(self):
		p1 = np.array([self.x[0],self.y[0]])
		p2 = np.array([self.x[-1],self.y[-1]])
		if (p1 == p2).all():
			p1 += 1e-5
		p3s = np.dstack((self.x[1:-1],self.y[1:-1]))[0]

		d = np.abs(p3s[:,0]*(p2[1] - p1[1]) - p3s[:,1]*(p2[0] - p1[0]) + p2[0]*p1[1] - p2[1]*p1[0])/(np.linalg.norm(p1-p2))
		try:
			i = d.argmax()
		except:
			return np.array(6*[0])
		if ((d - d[i]) == 0).all():
			i = int(len(self.x)/2)
		return np.array([self.x[0],self.y[0],self.x[i+1],self.y[i+1],self.x[-1],self.y[-1]])






	def __eq__(self, other):
		return self.id == other.id and self.x == other.x

	def __ne__(self,other):
		return not self == other

	def __hash__(self):
		return "Stroke(id=%s)" % self.id

	def center_distance(self, other):
		return np.sqrt((self.ymed - other.ymed)**2 + (self.xmed - other.xmed)**2)

	def closest_distance(self, other):
		XA = np.column_stack((self.x,self.y))
		XB = np.column_stack((other.x,other.y))
		dd = np.min(cdist(XA,XB))
		return dd
	def closest_distance_arg(self, other):
		XA = np.column_stack((self.x,self.y))
		XB = np.column_stack((other.x,other.y))
		dist = cdist(XA,XB)
		ind = np.argmin(dist)
		return (np.unravel_index(ind,dist.shape),np.min(dist))

	def farthest_distance(self, other):
		XA = np.column_stack((self.x,self.y))
		XB = np.column_stack((other.x,other.y))
		return np.max(cdist(XA,XB))

	def get_trace(self):
		return list(zip(self.x,self.y))

	def checkInBox(self, box):
		xmin,xmax,ymin,ymax = box
		xmed = (xmax + xmin)/2
		ymed = (ymax + ymin)/2
		width = xmax-xmin
		height = ymax-ymin
		return (abs(self.xmed - xmed) < (width + self.width)) and (abs(self.ymed - ymed) < (height + self.height))




	def get_angle_dist(self, stroke):
		dx = np.add(stroke.x,-self.xmed)
		dy = np.add(stroke.y,-self.ymed)
		r = np.sqrt(dx**2 + dy**2)
		r0 = np.where(r==0)
		r = np.delete(r,r0)
		theta = np.arctan2(dy,dx)
		theta = np.delete(theta,r0)
		np.putmask(theta,theta<0,2*np.pi+theta)
		return [r,theta]



	def leftOf(self,other):
		return other.xmin >= self.xmax
	def rightOf(self,other):
		return other.xmax <= self.xmin

	def above(self,other):
		return other.ymin >= self.ymax
	def below(self,other):
		return other.ymax <= self.ymin

	def bb_intersects(self, other):
		c1 = (self.xmin <= other.xmin <= self.xmax) and ((self.ymin <= other.ymin <= self.ymax) or other.ymin <= self.ymin <= other.ymax)
		c2 = (other.xmin <= self.xmin <= other.xmax) and ((self.ymin <= other.ymin <= self.ymax) or other.ymin <= self.ymin <= other.ymax)
		return c1 or c2

	def intersects(self, other):
		sp = []
		op = []
		for x,y in zip(self.x,self.y):
			sp.append((x,y))
		for x,y in zip(other.x,other.y):
			op.append((x,y))
		if len(op) == 1 or len(sp) == 1:
			return False
		a1 = LineString(sp)
		a2 = LineString(op)

		return a1.crosses(a2)



	def rotate(self, angle):
		theta = np.radians(angle)
		xy = np.dstack((self.x,self.y))[0]
		c, s = np.cos(theta), np.sin(theta)
		R = np.matrix('{} {}; {} {}'.format(c, -s, s, c))
		xy_ = R*xy.T
		self.x = np.array(xy_[0,:].T).flatten()
		self.y = np.array(xy_[1,:].T).flatten()
		self.setup()




	def simplify(self,factor = 0.01):
		coords = list(zip(self.x,self.y))
		simplified = simplify_coords(coords, factor)
		x = []
		y = []
		for X,Y in simplified:
			x.append(X)
			y.append(Y)
		x = np.array(x)
		y = np.array(y)
		self.x = x
		self.y = y
		self.setup()


	def hasDuplicatePoints(self):
		t = self.get_trace()
		s = set()
		for p in t:
			s.add(p)
		return len(t) != len(s)

	def removeDuplicatedPoints(self):
		t = self.get_trace()
		x = [t[0][0]]
		y = [t[0][1]]
		for t1,t2 in zip(t,t[1:]):
			if t1 == t2:
				continue
			else:
				x.append(t2[0])
				y.append(t2[1])
		self.x = np.array(x)
		self.y = np.array(y)
		self.setup()


	def smoothConv(self):
		if len(self.x) <= 2:
			return
		a,b = self.x[0],self.x[-1]
		c,d = self.y[0],self.y[-1]
		self.x = np.convolve(self.x, np.ones(3)/3,'valid')
		self.y = np.convolve(self.y, np.ones(3)/3,'valid')
		self.x = np.concatenate([[a],self.x,[b]]).flatten()
		self.y = np.concatenate([[c],self.y,[d]]).flatten()


	def smoothConvN(self,N):
		for x in range(N):
			self.smoothConv()
		self.setup()



	def remove_hooks(self):
		# Removed hooks common in digital writing.

		sharp_points=self.sharpPoints
		if len(self.sharpPoints) > 3:
			start = False
			end = False
			ld = np.sqrt(self.height**2 + self.width**2)
			betha_b0 =  self.slopeAngle(sharp_points[0][1], sharp_points[1][1])
			betha_b1 = self.slopeAngle(sharp_points[1][1], sharp_points[2][1]);
			lseg_b = self.distance(sharp_points[0][-1], sharp_points[1][-1])
			lambda_b = self.angularDifference(betha_b0, betha_b1)

			#At ending
			betha_e0 =  self.slopeAngle(sharp_points[-1][1], sharp_points[-2][1])
			betha_e1 = self.slopeAngle(sharp_points[-2][1], sharp_points[-3][1]);
			lseg_e = self.distance(sharp_points[-1][-1], sharp_points[-2][-1])
			lambda_e = self.angularDifference(betha_e0, betha_e1)


			if lambda_e > 0.5*np.pi / 4 and lseg_e < 0.07 * self.diag:
				end = True


			if lambda_b > 0.5*np.pi / 4 and lseg_b < 0.07 * self.diag:
				start = True


			if end:
				in1 = self.sharpPoints[-1][0]

			if start:
				in2 = self.sharpPoints[0][0]


			if end and not start:
				self.x = self.x[:in1]
				self.y = self.y[:in1]
			if not end and start:
				self.x = self.x[in2+1:]
				self.y = self.y[in2+1:]

			if end and start:
				self.x = self.x[in2+1:in1]
				self.y = self.y[in2+1:in1]
				

			self.setup()

	def removeHooks(self,sharp_points):
		if len(sharp_points) >= 4:
					#remove hooks....

					#Check for hooks in the segment b (beginning)
					#and at the segment e (ending)

					#get the diagonal length....
					minX, maxX, minY, maxY  = (self.xmin,self.xmax,self.ymin,self.ymax)
					ld = np.sqrt((minX-maxX)**2 + (minY-maxY)**2)

					#at beginning
					betha_b0 =  self.slopeAngle(sharp_points[0][1], sharp_points[1][1])
					betha_b1 = self.slopeAngle(sharp_points[1][1], sharp_points[2][1]);
					lseg_b = self.distance(sharp_points[0][-1], sharp_points[1][-1])
					lambda_b = self.angularDifference(betha_b0, betha_b1)

					#At ending
					betha_e0 =  self.slopeAngle(sharp_points[-1][1], sharp_points[-2][1])
					betha_e1 = self.slopeAngle(sharp_points[-2][1], sharp_points[-3][1]);
					lseg_e = self.distance(sharp_points[-1][-1], sharp_points[-2][-1])
					lambda_e = self.angularDifference(betha_e0, betha_e1)

					if lambda_e > np.pi / 4 and lseg_e < 0.07 * ld:
					#remove sharp point at the end...
						sharp_points.pop(-1)

					if lambda_b > np.pi / 4 and lseg_b < 0.07 * ld:
					#remove sharp point at the beginning
						sharp_points.pop(0)

		return sharp_points


	def slopeAngle(self, p1, p2):
		x = p2[0] - p1[0]
		y = p2[1] - p1[1]

		return np.arctan2(y, x)

	def distance(self, p1, p2):
		x = p2[0] - p1[0]
		y = p2[1] - p1[1]
		return np.sqrt(x**2 + y**2)





	def alt_histogram_2d(self, rows, cols):
		hist,_,_ = np.histogram2d(self.x,self.y,bins=(rows,cols),range=[[-1,1],[-1,1]])
		return hist.flatten()



	def histogram_2d(self, rows, cols):

		# if self.didhist:
		# 	return self.hist

		# else:
			#self.didhist = True
			x,y = (np.copy(self.x),np.copy(self.y))
			distribution = np.zeros((rows,cols))

			bin_size_x = 2. / (cols-1)
			bin_size_y = 2. / (rows-1)

			h_div = (x + 1.) / bin_size_x
			h_bin0 = np.int_(np.floor(h_div))
			ind_hbin0 = h_bin0 == cols - 1

			h_bin0[ind_hbin0] = cols - 2

			h_w1 = h_div - h_bin0
			h_w1[ind_hbin0] = 1

			h_bin1 = h_bin0 + 1

			v_div = (y + 1.) / bin_size_y
			v_bin0 = np.int_(np.floor(v_div))
			ind_vbin0 = v_bin0 == rows - 1

			v_bin0[ind_vbin0] = rows - 2

			v_w1 = v_div - v_bin0
			v_w1[ind_vbin0] = 1

			v_bin1 = v_bin0 + 1





			for v0,v1,h0,h1,hw,vw in zip(v_bin0,v_bin1,h_bin0,h_bin1,h_w1,v_w1):
				distribution[v0][h0] += ((1. - hw) * (1. - vw))
				distribution[v0][h1] += (hw * (1. - vw))
				distribution[v1][h0] += ((1. - hw) * vw)
				distribution[v1][h1] += (hw*vw)

			self.hist = distribution.flatten()
			return self.hist


	def addMissingPoints(self,epsilon):

		


		dr = np.sqrt((self.x[1:]-self.x[:-1])**2 + (self.y[1:]-self.y[:-1])**2).sum()/len(self.x)

		lx,ly = self.x[-1],self.y[-1]

		x = []
		y = []
		d = epsilon*dr
		points = self.get_trace()
		i = 0

		while i < len(points) - 1:
			if i > 1000:
				1/0
			n = 1
			lenght = self.distance(points[i],points[i+n])
			sum = 0

			while sum + lenght < d and i + n + 1 < len(points):
				n += 1
				sum += lenght
				lenght = self.distance(points[i + n - 1], points[i + n])

			diff = d - sum

			w2 = diff/(lenght + 1e-10)

			if w2 < 1.0:
				xp = points[i + n - 1][0] * (1 - w2) + points[i + n][0] * w2
				yp = points[i + n - 1][1] * (1 - w2) + points[i + n][1] * w2

				insert = True


				if i + n < len(points):
					if xp == points[i + n][0] and yp == points[i + n][1]:
						insert = False


				if insert:
					points.insert(i+n,(xp,yp))

			else:
				n += 1

			toErase = n - 1

			for j in range(toErase):
				del points[i + 1]

			i += 1

		x = []
		y = []
		for p in points:
			x.append(p[0])
			y.append(p[1])

		self.x = np.array(x)
		self.y = np.array(y)
		self.setup()







	def get_s_dist(self, i, j, points):
		a,c = points[i]
		b,d = points[j]

		return np.sqrt((a-b)**2 + (c-d)**2)

	def angularDifference(self, alpha, beta):
		diff = abs(alpha - beta)

		#circular ....
		while (diff > np.pi):
			diff = np.pi * 2 - diff

		return diff








	def getSharpPoints(self):


		points = self.get_trace()



		sharpPoints = [(0,points[0])]

		if len(self.x) <= 2:
			self.sharpPoints = sharpPoints
			return sharpPoints

		xb = self.x[1:]
		yb = self.y[1:]
		xa = self.x[:-1]
		ya = self.y[:-1]

		alpha = []
		for b,a in zip(xb-xa,yb-ya):
			alpha.append(atan2(a,b))

		theta = [alpha[0]-alpha[1]]

		for k in range(1, len(points) - 1):

			addPoint = False

			if k < len(points) - 2:
				theta.append(alpha[k] - alpha[k+1])

				if theta[k] != 0.0 and k > 0:
					delta = theta[k]*theta[k-1]

					if delta <= 0.0 and theta[k-1] > 0:
						addPoint = True

			phi = self.angularDifference(alpha[sharpPoints[-1][0]],alpha[k])

			if phi > np.pi:
				phi = np.pi * 2 - phi



			if phi >= np.pi/8:
				addPoint = True


			if addPoint:
				sharpPoints.append((k, points[k]))

		sharpPoints.append((len(points)-1,points[-1]))

		self.sharpPoints = sharpPoints

		self.theta = theta
		return sharpPoints



	def lerp(self, p1, p2, t):
		x = p1[0] * (1- t) + p2[0] * t
		y = p1[1] * (1 -t) + p2[1] * t

		return (x, y)


	def catmullRom(self, p1, p2, p3, p4, t):
			x1, y1 = p1
			x2, y2 = p2
			x3, y3 = p3
			x4, y4 = p4

			xt =  0.5 * ((-x1 + 3*x2 -3*x3 + x4)*t*t*t + (2*x1 -5*x2 + 4*x3 - x4)*t*t + (-x1+x3)*t + 2*x2)
			yt =  0.5 * ((-y1 + 3*y2 -3*y3 + y4)*t*t*t + (2*y1 -5*y2 + 4*y3 - y4)*t*t + (-y1+y3)*t + 2*y2)
			return (xt, yt)





	def splineResample(self,sharp_points,subDivisions):

		points = []


		if len(sharp_points) == 1:
			self.x = np.array([sharp_points[0][1][0]])
			self.y = np.array([sharp_points[0][1][1]])
			self.setup()
			return


		for i in range(len(sharp_points)):
			points.append(sharp_points[i][1])

			if i < len(sharp_points) - 1:
				innerPoints = (sharp_points[i + 1][0] - sharp_points[i][0]) * subDivisions
				tStep = 1.0 / innerPoints

			if i == 0 or i == len(sharp_points) - 2:
				for k in range(1, innerPoints):
					points.append(self.lerp(sharp_points[i][1], sharp_points[i + 1][1], tStep * k))


			elif i < len(sharp_points) - 2:
				for k in range(1, innerPoints):
					points.append(self.catmullRom(sharp_points[i - 1][1], sharp_points[i][1], sharp_points[i + 1][1], sharp_points[i + 2][1], tStep * k))

		x = []
		y = []
		for p in points:
			x.append(p[0])
			y.append(p[1])
		self.x = np.array(x)
		self.y = np.array(y)
		self.setup()




	def lineCumulativeFeatures(self):
		if False:
			return self.lcf
		else:
			self.didlcf = True
			totalAngularChange = 0
			lineLength = 0
			xb = self.x[1:]
			yb = self.y[1:]
			xa = self.x[:-1]
			ya = self.y[:-1]

			alpha = np.arctan2(yb-ya,xb-xa)

			theta = np.abs(np.ediff1d(alpha))

			np.putmask(theta,theta>np.pi,2*np.pi-theta)


			totalAngularChange = theta.sum()
			lineLength = np.sqrt((xa-xb)**2 + (ya-yb)**2).sum()

			self.lcf = np.array([totalAngularChange,lineLength,len(self.sharpPoints)])
			return self.lcf



	def alternateGabor(self, rows, cols):

			self.didgabor = True
			xb = self.x[1:]
			yb = self.y[1:]
			xa = self.x[:-1]
			ya = self.y[:-1]
			distances = np.sqrt((xa-xb)**2 + (ya-yb)**2)
			lineLength = distances.sum()
			if lineLength == 0:
				distribution = np.zeros(4*3*3)
				self.gabor = (distribution, lineLength)
				return self.gabor
			mid_X = (xa + xb)/2
			mid_Y = (ya + yb)/2
			p = np.arctan2(yb-ya,xb-xa) + np.pi
			da = np.append(mid_X,mid_Y)
			da = np.append(da, p)
			da = da.reshape((len(p),3))
			hist,_ = np.histogramdd(da, bins=(3,3,4), range=[[-1,1],[-1,1],[0,2*np.pi]], normed=False, weights=None)
			return (hist.flatten(),lineLength)


	def getGabor(self, rows, cols):



		if False:
			return self.gabor

		else:
			self.didgabor = True
			xb = self.x[1:]
			yb = self.y[1:]
			xa = self.x[:-1]
			ya = self.y[:-1]

			distribution = np.zeros(4*3*3)
			distances = np.sqrt((xa-xb)**2 + (ya-yb)**2)
			lineLength = distances.sum()
			if lineLength == 0:
				self.gabor = (distribution, lineLength)
				return self.gabor
			mid_X = (xa + xb)/2
			mid_Y = (ya + yb)/2
			p = np.arctan2(yb-ya,xb-xa) + np.pi

			#np.putmask(p, p < 0, np.pi + p)
			p /= (np.pi/4)



			wl = distances/lineLength
			val_y = ((mid_Y + 1.0) / 2.0) * (rows - 1)
			c0_y = val_y.astype(int)
			ind = c0_y >= rows-1
			c0_y[ind] = rows - 2
			c1_y = c0_y + 1
			w1_y = val_y - c0_y
			w0_y = 1 - w1_y

			val_x = ((mid_X + 1.0)/2.0)*(cols - 1)
			c0_x = val_x.astype(int)
			ind = c0_x >= cols-1
			c0_x[ind] = cols-2
			c1_x = c0_x + 1
			w1_x = val_x - c0_x
			w0_x = 1 - w1_x

			fp = np.floor(p)

			wp1 = p - fp

			p0 = (fp % 4).astype(int)

			p1 = ((p0 + 1) % 4).astype(int)

			g0 = wl * (1 - wp1)
			g1 = wl * wp1

			for c0x,c1x,c0y,c1y,G0,G1,w0x,w1x,w0y,w1y,P0,P1 in zip(c0_x,c1_x,c0_y,c1_y,g0,g1,w0_x,w1_x,w0_y,w1_y,p0,p1):
				distribution[(c0y *  cols + c0x) * 4 + P0] += (G0 * w0x * w0y)
				distribution[(c0y *  cols + c0x) * 4 + P1] += (G1 * w0x * w0y)
				#(0,1)
				distribution[(c1y *  cols + c0x) * 4 + P0] += (G0 * w0x * w1y)
				distribution[(c1y *  cols + c0x) * 4 + P1] += (G1 * w0x * w1y)
				#(1,0)
				distribution[(c0y *  cols + c1x) * 4 + P0] += (G0 * w1x * w0y)
				distribution[(c0y *  cols + c1x) * 4 + P1] += (G1 * w1x * w0y)
				#(1,1)
				distribution[(c1y *  cols + c1x) * 4 + P0] += (G0 * w1x * w1y)
				distribution[(c1y *  cols + c1x) * 4 + P1] += (G1 * w1x * w1y)




			self.gabor = (distribution, lineLength)
			return self.gabor


def angle(v1,v2):
	l1 = np.linalg.norm(v1)
	l2 = np.linalg.norm(v2)
	if l1*l2:
		dotp = np.dot(v1,v2)/(l1*l2)
		if dotp >= 1:
			return 0
		elif dotp <= -1:
			return np.pi
		else:
			return np.arccos(dotp)




if __name__ == '__main__':
	import matplotlib.pyplot as plt
	import timeit

	a = [(698.0, 114.0), (700.0, 114.0), (703.0, 116.0), (707.0, 121.0), (697.0, 120.0), (696.0, 120.0), (694.0, 121.0), (692.0, 121.0), (690.0, 122.0), (688.0, 123.0), (686.0, 125.0), (685.0, 126.0), (683.0, 128.0), (681.0, 129.0), (680.0, 131.0), (679.0, 133.0), (678.0, 135.0), (678.0, 137.0), (678.0, 139.0), (678.0, 140.0), (679.0, 142.0), (680.0, 143.0), (681.0, 144.0), (682.0, 145.0), (684.0, 145.0), (686.0, 145.0), (687.0, 145.0), (689.0, 145.0), (692.0, 144.0), (694.0, 143.0), (695.0, 142.0), (697.0, 140.0), (699.0, 138.0), (701.0, 136.0), (702.0, 134.0), (704.0, 132.0), (705.0, 129.0), (706.0, 127.0), (707.0, 125.0), (707.0, 123.0), (707.0, 122.0), (708.0, 121.0), (708.0, 120.0), (708.0, 121.0), (708.0, 122.0), (707.0, 123.0), (707.0, 125.0), (707.0, 127.0), (706.0, 129.0), (706.0, 131.0), (707.0, 133.0), (707.0, 135.0), (708.0, 137.0), (709.0, 139.0), (710.0, 140.0), (712.0, 141.0), (714.0, 141.0), (716.0, 142.0), (718.0, 142.0), (720.0, 142.0), (721.0, 142.0), (723.0, 141.0), (724.0, 141.0), (726.0, 140.0), (727.0, 140.0), (727.0, 139.0), (728.0, 139.0), (729.0, 138.0)]
