# Math Handwriting Recognition
Author: Hersh Patel

- - - -

## Instructions
Poetry, a virtual environment manager is required to install dependancies correctly. Install instructions: [Introduction | Documentation | Poetry - Python dependency management and packaging made easy.](https://python-poetry.org/docs/)


```
cd abacus-env
poetry install # Should take a few minutes
poetry shell
cd ../
python3 draw.py # Should just work!
```


